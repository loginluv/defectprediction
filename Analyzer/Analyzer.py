import csv
import operator
import ast
import os

fileList = ["tensorflow","docker","cygwin","cmake","socketio", "django", "scikit","react","postgre","hadoop"]

LABELING = "HAVG12__HAVG"
overallInfo = open('Overall_'+LABELING+'.csv', 'w')
csv_overall_writer = csv.writer(overallInfo, dialect="excel")

for filename in fileList :
    print "---------------------------------------"
    print "processing "+filename 
    print "---------------------------------------"
    f = open(filename + '.csv', 'r')
    # LABELING = "AAVG6__AAVG"
    DIR =LABELING + "/"    
    FILE_DIR = LABELING + "_FILE/"
    if not os.path.exists(DIR):
        os.mkdir(DIR)
    modifiedfilename = filename + "__"+LABELING

    if not os.path.exists(FILE_DIR):
        os.mkdir(FILE_DIR)
    fileinfodir = filename + "__"+LABELING +"_FILE"

    output = open(DIR + modifiedfilename + '.csv', 'w')
    fileInfo = open(FILE_DIR + fileinfodir + '.csv', 'w')
    
    rdr = csv.reader(f)
    rowlist = list(rdr)

