import csv
import operator 
import ast
import os

fileList = ["tensorflow","docker","cygwin","cmake","socketio", "django", "scikit","react","postgre","hadoop"]
LABELING = "NEW_HAVG3_HAVG"
overallInfo = open('Overall_'+LABELING+'.csv', 'w')
csv_overall_writer = csv.writer(overallInfo, dialect="excel")
csv_overall_writer.writerow(["filename","ChangeCnt","DefectChangeCnt","MergeCnt","FixCnt","BadFixCnt","ZeroDRECnt","ZeroBFRCnt","HDRECnt","HBFRCnt","AvgDRE","AvgBFR","avgDREwithoutZero","avgBFRwithoutZero"])
for filename in fileList :
    print "---------------------------------------"
    print "Analyzing..... "+filename
    print "---------------------------------------"
    f = open("../DP_csv/dataset/INPUTDATA/"+LABELING+"/"+filename +"__"+LABELING+ '.csv', 'r')
    DIR =LABELING + "/"    
    rdr = csv.reader(f)
    rowlist = list(rdr)
    MergeCnt = 0    
    ChangeCnt = 0
    AvgDRE = 0.0
    AvgBFR = 0.0
    accumDRE = 0.0
    accumBFR = 0.0
    FixCnt = 0
    BadFixCnt = 0
    DefectChangeCnt = 0
    ZeroDRECnt = 0
    ZeroBFRCnt = 0
    NonzeroDRECnt = 0
    NonzeroBFRCnt = 0
    HDRECnt = 0
    HBFRCnt = 0
    avgDREwithoutZero = 0.0
    avgBFRwithoutZero = 0.0
    for i in reversed(range(1,len(rowlist))):
        ChangeCnt += 1
        if ( rowlist[i][7] == "Merge"):
            MergeCnt += 1
            continue
        # defect count if defect add fix hashsed to the pool for each file
        if ( rowlist[i][9] == "TRUE" or rowlist[i][9] == "True"):
            DefectChangeCnt += 1
        # Refresh if the activity length more than 3 month
        # 1month = 2592000     3month =7776000  6month = 15552000 1y = 31104000
        #for eachFile in refineList:
        #    if (int(rowlist[i][2]) -  int(filelist[eachFile][3]) > 31104000 ):
                # Refresh time
            # if the developer fix, count +1
        # Badfix part
        if ( (rowlist[i][6] == "TRUE" or rowlist[i][6] == "True") ):
            FixCnt +=1
            if ( rowlist[i][9] == "TRUE" or rowlist[i][9] == "True"):
                BadFixCnt += 1
        # Check DRE value
        if (rowlist[i][29] == "0.0") :
            ZeroDRECnt += 1
        else:
            NonzeroDRECnt += 1
            avgDREwithoutZero += float(rowlist[i][29])

        if (rowlist[i][29] == "0.5"):
            HDRECnt += 1
        accumDRE += float(rowlist[i][29])

        # Check BFR value
        if (rowlist[i][30] == "0.0") :
            ZeroBFRCnt += 1
        else:
            NonzeroBFRCnt += 1 
            avgBFRwithoutZero += float(rowlist[i][30])

        if (rowlist[i][30] == "0.5"):
            HBFRCnt += 1

        accumBFR += float(rowlist[i][30])
    AvgDRE = accumDRE / float(len(rowlist)) 
    AvgBFR = accumBFR / float(len(rowlist))
    avgDREwithoutZero = avgDREwithoutZero/float(NonzeroDRECnt)
    avgBFRwithoutZero = avgBFRwithoutZero/float(NonzeroBFRCnt)

    # file/ChangeCnt/DefectChangeCnt/MergeCnt/FixCnt/BadFixCnt/ZeroDRECnt/ZeroBFRCnt/HDRECnt/HBFRCnt/AvgDRE/AvgBFR/avgDREwithoutZero/avgBFRwithoutZero
    csv_overall_writer.writerow([filename,ChangeCnt,DefectChangeCnt,MergeCnt,FixCnt,BadFixCnt,ZeroDRECnt,ZeroBFRCnt,HDRECnt,HBFRCnt,AvgDRE,AvgBFR,avgDREwithoutZero,avgBFRwithoutZero])
    f.close()
    print "Done"
overallInfo.close()
