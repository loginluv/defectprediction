import csv
import sys
from MyResearchLR.DataPreparation.normarlization import Logarithm_Normalization
from MyResearchLR.Validation.sensitivityAnalysis import SensitivityAnalysis
from MyResearchLR.Validation.crossValidation import CrossValidation

fileList = ["tensorflow", "docker", "cygwin", "cmake", "socketio", "tesseract","django","scikit","notepad"]
# fileList = ["scikit"]
# fileList = ["notepad"]
#fileList = ["notepad", "django"]
#fileList = ["django"]
#fileList = ["docker"]
# "docker", "socketio", "notepad", "scikit", "django", "tesseract", "cmake",

csv.field_size_limit(sys.maxsize)

for filename in fileList:
    matrix = []
    cleanMat = []
    buggyMat = []
    reSampledMat = []
    normalizedMat = []

    f = open('../new_input/' + filename + '_BF.csv', 'r')

    csvReader = csv.reader(f)

    buggyCnt = 0
    cleanCnt = 0

    print "Show Overall status and make VIF CSV"

    inputMat = []
    for row in csvReader:
        if row[0] != 'commit_hash':
            if row[9] == 'True' or row[9] == 'TRUE':
                buggyCnt = buggyCnt + 1
                buggyMat.append(row)
            else :
                cleanCnt = cleanCnt + 1
                cleanMat.append(row)
            inputMat.append(row)
    f.close()
    print filename
    print "Initial Commit" + row[4] 
    print "%s %d %s %d" % ("Buggy Class : ", buggyCnt, ", Clean Class : ", cleanCnt)
    print "Total : ", buggyCnt+cleanCnt, "percent of buggy class : ", 100*buggyCnt/float(buggyCnt+cleanCnt)
    #Data random under sampling process
    # reSampledMat = RandomUnderSampling(cleanMat, buggyCnt)
    # reSampledMat.extend(buggyMat)
    # print "%s %d" % ("After Re-sampling Merged List Length : ", len(reSampledMat))

    logNormalizedMat = Logarithm_Normalization(inputMat)
    print "%s %d" % ("Normalized Mat Length : ", len(logNormalizedMat[0]))
    print "=================================="
    print " "

    # Write Code for writing Normalized Data into CSV File for VIF check in R studio
    with open('../new_input/BADFIX_VIF/vif_' + filename + ".csv", "w") as file:
         csv_writer = csv.writer(file, dialect="excel")
         label = ["NS", "NF", "Entropy", "LA_LT", "LD_LT", "LT_NF", "NDEV", "AGE", "NUC_NF", "EXP", "SEXP", "NPF", "NPBC", "BADFIX","FIX", "BUGGY", "TimeStamp"]
         csv_writer.writerow(label)#
         for line in logNormalizedMat:
             csv_writer.writerow(line)
    print "Done"
    file.close()


