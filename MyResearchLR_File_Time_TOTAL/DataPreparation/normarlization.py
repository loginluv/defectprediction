import math
import sys

"""
CSV File Index List
[0] - Commit Hash   [1] - Author Name           [2] - Author Date Unix Time Stamp
[3] - Author Email  [4] - Author Commit Date    [5] - Commit Message
[6] - Fix (Boolean) [7] - Commit Classification (Merge, Feature Addition, Corrective, None)
[8] - Linked (X)    [9] - Contains Bug          [10] - Fixes
[11] - NS (Number of Subsystems)
[12] - ND ( Number of Directories)      [13] - NF (Number of Files)
[14] - Entropy                          [15] - LA (Lines Added)             [16] - LD (Lines Deleted)
[17] - List of Files Changed            [18] - LT (Lines of code in a file before the change)
[19] - NDEV (Number of Developers changed file before)      [20] AGE - Average time interval between commit
[21] - NUC (Number of Unique Change)    [22] - EXP (Developer Experiences)  [23] - REXP [24] - SEXP
[25] - NPF (Number of Previous Fault of included file)      [26] - NPBC (Number of Previous Buggy Commit of developer)
[27] - GLM Probability                  [28] - Repository ID
[29] - BADFIX
"""


# Used Metric /NS/NF/Entropy/LA_LT/LD_LT/LT_NF/NDEV/AGE/NUC_NF/EXP/SEXP/NPF/NPBC/FIX/BUGGY

def Logarithm_Normalization (targetMat) :
    retMat = []

    NS, NF, Entropy, LA, LD, LT, NDEV, AGE, NUC, EXP, SEXP, NPF, NPBC, FIX, DRE, BADFIX = (1,) * 16
    LA_LT, LD_LT, LT_NF, NUC_NF, unixTimeStamp = (1,) * 5


    for row in targetMat:
        tempInstance = []
        NS = num(row[11])
        NF = num(row[13])
        Entropy = num(row[14])
        LA = num(row[15])
        LD = num(row[16])
        LT = num(row [18])
        NDEV = num(row[19])
        NUC = num(row[21])
        AGE = num(row[20])
        EXP = num(row[22])
        SEXP = num(row[24])
        NPF = num(row[25])
        NPBC = num(row[26])
        DRE = num(row[29])
        BADFIX = num(row[30])
        unixTimeStamp = num(row[2])

        if row[6] == 'True' or row[6] == 'TRUE':
            FIX = 1
        else:
            FIX = 0

        if row[9] == 'True' or row[9] == 'TRUE':
            BUGGY = 1
        else:
            BUGGY = 0

        if LT != 0:
            LA_LT = LA / LT
            LD_LT = LD / LT
        else:
        # Log (1) = 0
            LA_LT = 1
            LD_LT = 1
        if NF != 0:
            if LT == 0:
                LT_NF = 1
            else:
                LT_NF = LT / NF
            if NUC == 0:
                NUC_NF = 1
            else:
                NUC_NF = NUC / NF
        else:
            LT_NF = 1
            NUC_NF = 1

        try:
            NPBC = (NPBC / float(EXP)) * 100
        except ZeroDivisionError:
            NPBC = 1
        # Multiply 100 for transform NPF metric into percentage
        NPF = NPF * 100

        NS = log(NS)
        NF = log(NF)
        Entropy = log(Entropy)
        LA_LT = log(LA_LT)
        LD_LT = log(LD_LT)
        LT_NF = log(LT_NF)
        NDEV = log(NDEV)
        AGE = log(AGE)
        NUC_NF = log(NUC_NF)
        EXP = log(EXP)
        SEXP = log(SEXP)
        NPF = log(NPF)
        NPBC = log(NPBC)
        # Add new feature
        DRE = log(DRE*100)
        BADFIX = log(100*BADFIX)

        tempInstance.append(NS) #0
        tempInstance.append(NF) #1
        tempInstance.append(Entropy) #2
        tempInstance.append(LA_LT) #3
        tempInstance.append(LD_LT) #4
        tempInstance.append(LT_NF) #5
        tempInstance.append(NDEV) #6
        tempInstance.append(AGE) #7
        tempInstance.append(NUC_NF) #8
        tempInstance.append(EXP) #9
        tempInstance.append(SEXP) #10
        tempInstance.append(NPF) #11
        tempInstance.append(NPBC) #12
        tempInstance.append(DRE) #13
        tempInstance.append(BADFIX) #14
        tempInstance.append(FIX) #15
        tempInstance.append(BUGGY) #16
        tempInstance.append(unixTimeStamp)
        retMat.append(tempInstance)
    return retMat
#Normalization process. Translate string to float value
def num(s):
    ret = 0
    try:
        ret = float(s)
    except:
        print sys.exc_info()[0]
    return ret

def minus(max, min):
    if max == min:
        return 1
    else :
        return max - min

def log(x):
    # The author of TSE 2013 paper applied standard log transformation for normalization in data pre-processing
    try:
        ret = math.log10(x)
    except:
        ret = 0
    return ret
