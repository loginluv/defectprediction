from MyResearchLR_File_Time_TOTAL.DataPreparation.randomSampling import RandomUnderSampling
from sklearn.cross_validation import train_test_split

from MyResearchLR_File_Time_TOTAL.ModelConstruction.LogisticRegressionModel import LogisticRegressionModelConstruct


def CrossValidation (targetMat, numOfFold, fileName):

    #train, test = train_test_split(targetMat, test_size=0.1)
    print "Cross Validation"
    splitedMat = []
    trainMat = targetMat
    testMat = []
    for index in range(9):
        divIndex = index
        trainIndex = 10 - divIndex
        trainSize = 1 / float(trainIndex)
        #print trainSize
        trainMat, testMat = train_test_split(trainMat, test_size=trainSize)
        splitedMat.append(testMat)

    splitedMat.append(trainMat)
    # total input matrix split into num of fold subset of matrix

    accuracy = []
    precision = []
    recall = []
    f1score = []
    auc = []
    tp = []
    tn = []
    fp = []
    fn = []
    trainDataLength= []
    testDataLength = []
    trainCleanCnt = []
    trainBuggyCnt = []
    testCleanCnt = [] 
    testBuggyCnt = []

    for i in range(9):
        test = splitedMat[i+1]
        train = []
        for index in range(i+1):
            train += splitedMat[index]

        # Source Code for Re-sampling train data
        buggyMat = []
        cleanMat = []
        cleanCnt = 0
        buggyCnt = 0

        for row in train:
            #change index num
            if row[16] == 1 :
                buggyCnt = buggyCnt + 1
                buggyMat.append(row)
            else:
                cleanCnt = cleanCnt + 1
                cleanMat.append(row)
        print "Buggy Count : ", buggyCnt, "Clean Count : ", cleanCnt


        # Majority and minority class can be change depends on fold of data
        if buggyCnt > cleanCnt:
            reSampledMat = RandomUnderSampling(buggyMat, cleanCnt)
            reSampledMat.extend(cleanMat)
        elif buggyCnt < cleanCnt:
            reSampledMat = RandomUnderSampling(cleanMat, buggyCnt)
            reSampledMat.extend(buggyMat)
        else:
            reSampledMat = buggyMat
            reSampledMat.extend(cleanMat)
        print "%s %d" % ("After Re-sampling Merged List Length : ", len(reSampledMat))

        t_trainCleanCnt = 0
        t_trainBuggyCnt = 0
        for row in reSampledMat:
            if row[16] == 1:
                t_trainBuggyCnt = t_trainBuggyCnt + 1
            else:
                t_trainCleanCnt = t_trainCleanCnt + 1

        t_testBuggyCnt = 0
        t_testCleanCnt = 0

        for row in test:
            if row[16] == 1:
                t_testBuggyCnt = t_testBuggyCnt + 1

            else:
                t_testCleanCnt = t_testCleanCnt + 1

        trainCleanCnt.append(t_trainCleanCnt)
        trainBuggyCnt.append(t_trainBuggyCnt)
        testBuggyCnt.append(t_testBuggyCnt)
        testCleanCnt.append(t_testCleanCnt)
        trainDataLength.append(len(reSampledMat))
        testDataLength.append(len(test))

        Accuracy, Precision, Recall, F1score, AUC, TP, TN, FP, FN = LogisticRegressionModelConstruct(reSampledMat,test, fileName)
        accuracy.append(Accuracy)
        precision.append(Precision)
        recall.append(Recall)
        f1score.append(F1score)
        auc.append(AUC)
        tp.append(TP)
        tn.append(TN)
        fp.append(FP)
        fn.append(FN)

    print "Acc      Pre    Recall    F1     AUC"
    for i in range(9):
        print "%.2f\t%.2f\t%.2f\t%.2f\t%.2f" % (accuracy[i], precision[i], recall[i], f1score[i], auc[i])

    return trainDataLength, testDataLength, testCleanCnt, testBuggyCnt, accuracy, precision, recall, f1score, \
           auc, tp, tn, fp, fn


if __name__ == '__main__':
    dummyArr = [i for i in range(100)]
    dummy = CrossValidation(dummyArr, 10)

