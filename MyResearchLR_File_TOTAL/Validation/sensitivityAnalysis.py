from Utils.randomSampling import RandomUnderSampling
from EnhancedLogisticRegression.ModelConstruction.LogisticRegressionModel import LogisticRegressionModelConstruct
from EnhancedLogisticRegression.ModelConstruction.SoftMax import SoftMaxClassification

def SensitivityAnalysis(targetData, trainPercent, fileName):
    accuracy = 0
    print "Sensitivity Analysis"
    # Iterable elements for 1st param, getKey function of each element (Unix Time Stamp)
    print "Time Stamp"
    sortedMat = sorted(targetData, key=getKey)

    totalIndex = len(sortedMat)
    trainIntervalPercent = 0.2
    trainingStartIndex = int(len(sortedMat) * trainPercent)
    trainingEndIndex = int(len(sortedMat) * (trainPercent + trainIntervalPercent))
    testPercent = trainPercent + trainIntervalPercent + 0.1

    testIndex = int(len(sortedMat) * testPercent)

    train = []
    test = []
    print "Training Start Index : ", trainingStartIndex, "Training End Index & Test Start : ", trainingEndIndex, "Test End Index : ", testIndex
    for i in range(trainingStartIndex, trainingEndIndex):
        train.append(sortedMat[i])

    # Source code to predict after 10 percent of data
    for i in range(trainingEndIndex, testIndex):
        test.append(sortedMat[i])

    # Source code to predict all data
    # for i in range(trainingIndex, totalIndex):
    #     test.append(sortedMat[i])

    # Source Code for Re-sampling train data
    buggyMat = []
    cleanMat = []
    cleanCnt = 0
    buggyCnt = 0

    for row in train:

        if row[14] == 1:
            buggyCnt = buggyCnt + 1
            buggyMat.append(row)
        else:
            cleanCnt = cleanCnt + 1
            cleanMat.append(row)
    print "Buggy Count : ", buggyCnt, "Clean Count : ", cleanCnt

    if buggyCnt > cleanCnt :
        reSampledMat = RandomUnderSampling(buggyMat, cleanCnt)
        reSampledMat.extend(cleanMat)
    elif buggyCnt < cleanCnt:
        reSampledMat = RandomUnderSampling(cleanMat, buggyCnt)
        reSampledMat.extend(buggyMat)
    else:
        reSampledMat = buggyMat
        reSampledMat.extend(cleanMat)

    trainCleanCnt = 0
    trainBuggyCnt = 0
    for row in reSampledMat:
        if row[14] == 1:
            trainBuggyCnt = trainBuggyCnt + 1

        else:
            trainCleanCnt = trainCleanCnt + 1

    testBuggyCnt = 0
    testCleanCnt = 0

    for row in test:
        if row[14] == 1:
            testBuggyCnt = testBuggyCnt + 1

        else:
            testCleanCnt = testCleanCnt + 1


    trainDataLength = len(reSampledMat)

    testDataLength = len(test)

    print "%s %d" % ("After Re-sampling Merged List Length : ", len(reSampledMat))

    print "%s %d %s %d" % ("Train Length : ", len(reSampledMat), "Test Length : ", len(test))
    print "Train Clean Cnt : ", str(trainCleanCnt), "Train Buggy Cnt : ", str(trainBuggyCnt), "Test Clean Cnt : ", str(testCleanCnt), "Test Buggy Cnt : ", str(testBuggyCnt)

    accuracy = []
    precision = []
    recall = []
    f1score = []
    auc = []
    tp = []
    tn = []
    fp = []
    fn = []

    for i in range(10):
        print i
        Accuracy, Precision, Recall, F1score, AUC,TP, TN, FP, FN = LogisticRegressionModelConstruct(reSampledMat, test, fileName)
        #Accuracy, Precision, Recall, F1score, AUC = SoftMaxClassification(reSampledMat, test, fileName)

        accuracy.append(Accuracy)
        precision.append(Precision)
        recall.append(Recall)
        f1score.append(F1score)
        auc.append(AUC)
        tp.append(TP)
        tn.append(TN)
        fp.append(FP)
        fn.append(FN)
        #accuracy.append(LogisticRegressionModel.LogisticRegressionModelConstruct(train,test))

    for i in range(10):
        print "%.2f\t%.2f\t%.2f\t%.2f\t%.2f" % (accuracy[i], precision[i], recall[i], f1score[i], auc[i])

    return trainDataLength, testDataLength, testCleanCnt, testBuggyCnt, accuracy, precision, recall, f1score, \
           auc, tp, tn, fp, fn


# Return each commit's unix timestamp for sorting
def getKey(dataInstance):
    return dataInstance[15]
