import numpy as np
import tensorflow as tf
from sklearn.metrics import roc_auc_score

from Utils.ColumnFilter import ColumnFiltering


def ListToNumpyArray(targetData):
    inputMat = []
    outputMat = []
    # Used Metric /NS/NF/Entropy/LA_LT/LD_LT/LT_NF/NDEV/AGE/NUC_NF/EXP/SEXP/NPF/NPBC/FIX/BUGGY
    bias = []
    NS = []
    NF = []
    Entropy = []
    LA_LT = []
    LD_LT = []
    LT_NF = []
    NDEV = []
    AGE = []
    NUC_NF = []
    EXP = []
    SEXP = []
    # NPF = []
    # NPBC = []
    FIX = []
    mergedList = []

    for row in targetData:
        # tempInstance.append(NS)
        # tempInstance.append(NF)
        # tempInstance.append(Entropy)
        # tempInstance.append(LA_LT)
        # tempInstance.append(LD_LT)
        # tempInstance.append(LT_NF)
        # tempInstance.append(NDEV)
        # tempInstance.append(AGE)
        # tempInstance.append(NUC_NF)
        # tempInstance.append(EXP)
        # tempInstance.append(SEXP)
        # # tempInstance.append(NPF)
        # # tempInstance.append(NPBC)
        # tempInstance.append(FIX)
        # tempInstance.append(BUGGY)
        # # Time Stamp Index => 13
        # tempInstance.append(UnixTimeStamp)
        bias.append(1)
        NS.append(row[0])
        NF.append(row[1])
        Entropy.append(row[2])
        LA_LT.append(row[3])
        LD_LT.append(row[4])
        LT_NF.append(row[5])
        NDEV.append(row[6])
        AGE.append(row[7])
        NUC_NF.append(row[8])
        EXP.append(row[9])
        SEXP.append(row[10])
        # NPF.append(row[11])
        # NPBC.append(row[12])
        FIX.append(row[11])

        # Append Buggy Label to output list
        outputMat.append(row[12])
    mergedList.append(bias)
    mergedList.append(NS)
    mergedList.append(NF)
    mergedList.append(Entropy)
    mergedList.append(LA_LT)
    mergedList.append(LD_LT)
    mergedList.append(LT_NF)
    mergedList.append(NDEV)
    mergedList.append(AGE)
    mergedList.append(NUC_NF)
    mergedList.append(EXP)
    mergedList.append(SEXP)
    # mergedList.append(NPF)
    # mergedList.append(NPBC)
    mergedList.append(FIX)

    train_np = np.array(mergedList)

    inputMat = train_np[0:]
    outputMat = np.array(outputMat)

    return inputMat, outputMat

def LogisticRegressionModelConstruct(trainData, testData, fileName):

    train_input_data, train_output_data = ListToNumpyArray(trainData)
    print "train number of feature"
    print len(train_input_data)
    test_input_data, test_output_data = ListToNumpyArray(testData)

    """
        [0]-bias  [1]-NS     [2]-NF    [3]-Entropy  [4]-LA_LT
        [5]-LDLT  [6]-LTNF   [7]-NDEV  [8]-AGE      [9]-NUCNF
        [10]-EXP    [11]-SEXP   [12]-NPF    [13]-NPBC   [14]-FIX
    """

    # In original model, there is no advanced feature because they are excluded in normalization process
    """
    tensorflow_RemoveIndex = [10]    # EXP
    docker_RemoveIndex = [6,7,10]    # LTNF, NDEV, EXP
    cygwin_RemoveIndex = [7,11]      # NDEV, SEXP
    cmake_RemoveIndex = [10]         # EXP
    socketIO_RemoveIndex = [10]      # EXP
    tesseract_RemoveIndex = []   
    django_RemoveIndex = [7, 10]     # NDEV, EXP
    scikit_RemoveIndex = [10]        # EXP
    notepad_RemoveIndex = [11]       # SEXP
    react_RemoveIndex = [7]          # NDEV
    angular_RemoveIndex = [7]        # NDEV
    bugzilla_RemoveIndex = [11]      # SEXP

    """
    """
    tensorflow_RemoveIndex = [11]
    docker_RemoveIndex = [10,7,9]
    cygwin_RemoveIndex = [7,11]
    cmake_RemoveIndex = [11]
    socketIO_RemoveIndex = [11]
    tesseract_RemoveIndex = [11]
    django_RemoveIndex = [11]
    notepad_RemoveIndex = [11]
    scikit_RemoveIndex = [11]
    react_RemoveIndex = [7]
    angular_RemoveIndex = [7]
    bugzilla_RemoveIndex = [11]
    """
    tensorflow_RemoveIndex = [11]
    docker_RemoveIndex = [10,7,9]
    cygwin_RemoveIndex = [11]
    cmake_RemoveIndex = [11]
    socketIO_RemoveIndex = [11]
    tesseract_RemoveIndex = [1,11]
    django_RemoveIndex = [11]
    notepad_RemoveIndex = [11]
    scikit_RemoveIndex = [11]
    react_RemoveIndex = [7,11]
    angular_RemoveIndex = []
    bugzilla_RemoveIndex = [11]
    vscode_RemoveIndex = [11]
    hadoop_RemoveIndex = []
    postgre_RemoveIndex = []

    # Choose which data set used for prediction

    if fileName == 'tensorflow':
        removeMat = tensorflow_RemoveIndex
    elif fileName == 'tesseract':
        removeMat = tesseract_RemoveIndex
    elif fileName == 'scikit':
        removeMat = scikit_RemoveIndex
    elif fileName == 'socketio':
        removeMat = socketIO_RemoveIndex
    elif fileName == 'django':
        removeMat = django_RemoveIndex
    elif fileName == 'notepad':
        removeMat = notepad_RemoveIndex
    elif fileName == 'cmake':
        removeMat = cmake_RemoveIndex
    elif fileName == 'cygwin':
        removeMat = cygwin_RemoveIndex
    elif fileName == 'docker':
        removeMat = docker_RemoveIndex
    elif fileName == 'react':
        removeMat = react_RemoveIndex
    elif fileName == 'angular':
        removeMat = angular_RemoveIndex
    elif fileName == 'bugzilla':
        removeMat = bugzilla_RemoveIndex
    elif fileName == 'hadoop':
        removeMat = hadoop_RemoveIndex
    elif fileName == 'vscode':
        removeMat = vscode_RemoveIndex
    else :
        removeMat = postgre_RemoveIndex

    trainInputFilteredMat = ColumnFiltering(train_input_data, removeMat)

    testInputFilteredMat = ColumnFiltering(test_input_data, removeMat)

    X = tf.placeholder(tf.float32)
    Y = tf.placeholder(tf.float32)

    W = tf.Variable(tf.random_uniform([1, len(trainInputFilteredMat)], -1.0, 1.0))

    h = tf.matmul(W, X)
    hypothesis = tf.div(1., 1.+tf.exp(-h))

    cost = -tf.reduce_mean(Y*tf.log(hypothesis) + (1-Y)*tf.log(1-hypothesis))

    a = tf.Variable(0.001)
#    optimizer = tf.train.GradientDescentOptimizer(a)
    optimizer = tf.train.AdamOptimizer(a)

    train = optimizer.minimize(cost)

    init = tf.initialize_all_variables()

    sess = tf.Session()
    sess.run(init)

    for step in xrange(2000):
        sess.run(train, feed_dict={X:trainInputFilteredMat, Y:train_output_data})
        if step % 200 ==0:
            print step, sess.run(cost, feed_dict={X:trainInputFilteredMat, Y:train_output_data})
        #     continue
    results = sess.run(hypothesis, feed_dict={X:testInputFilteredMat})
    i = 0
    correct = 0
    total = 0
    TruePositive = 0
    TrueNegative = 0
    FalsePositive = 0
    FalseNegative = 0
    #print results
    for result in results[0]:
        if (result >= 0.5).all():
            #Answer = True, Prediction = Positive
            #TruePositive
            if test_output_data[i] == 1:
                correct += 1
                TruePositive +=1
            #Answer = False, Prediction = Positive
            #FalsePositive
            else:
                FalsePositive +=1

        elif (result <0.5).all():
            #Answer = False, Prediction = Negative
            if test_output_data[i] == 0:
                correct += 1
                TrueNegative += 1

            #Answer = True, Prediction = Negative
            else:
                FalseNegative += 1

        total += 1
        i+=1

    print 'TP = ' + str(TruePositive) + ", TN = " + str(TrueNegative) + ", FP = " \
          + str(FalsePositive) + ", FN = " + str(FalseNegative)
    print "Defective Change : ", TruePositive + FalseNegative, ", Non-Defective Change : ", FalsePositive + TrueNegative
    try:
        Accuracy = (TruePositive + TrueNegative) / float(
            TruePositive + TrueNegative + FalseNegative + FalsePositive) * 100
    except ZeroDivisionError:
        Accuracy = 0
    try:
        Recall = (TruePositive / float(TruePositive + FalseNegative)) * 100
    except ZeroDivisionError:
        Recall = 0
    try:
        Precision = (TruePositive / float(TruePositive + FalsePositive)) * 100
    except ZeroDivisionError:
        Precision = 0
    try:
        F1Score = 2 * Precision * Recall / float(Precision + Recall)
    except ZeroDivisionError:
        F1Score = 0

    print 'Total Count = ' + str(total) + ', Correct = ' + str(correct)
    print 'Accuracy = ' + str(Accuracy) + ', Precision = ' + str(Precision) + ', Recall = ' + str(Recall) + ', F1 Score = ' + str(F1Score)

    y_true = np.array(test_output_data)
    y_score = np.array(results[0])
    try :
        aucScore = roc_auc_score(y_true, y_score) * 100
    except ValueError:
        aucScore = 0

    return Accuracy, Precision, Recall, F1Score, aucScore, TruePositive, TrueNegative, FalsePositive, FalseNegative
