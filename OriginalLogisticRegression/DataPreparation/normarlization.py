import math
import sys

def MinMax_Normalization (targetMat):
    retMat = []
    floatMat = []

    maxNS, minNS, maxND, minND, maxNF, minNF, maxEntropy, minEntropy, maxLA_LT, minLA_LT, maxLD_LT, minLD_LT = (1,) * 12
    maxLT_NF, minLT_NF, maxFIX, minFIX, maxNDEV, minNDEV, maxAGE, minAGE, maxNUC_NF, minNUC_NF, maxEXP, minEXP = (1,) * 12
    maxREXP, minREXP, maxSEXP, minSEXP = (1,) * 4

    NS, NF, Entropy, LA, LD, LT, NDEV, AGE, NUC, EXP, SEXP, NPF, NPBC, FIX = (1,) * 14
    LA_LT, LD_LT, LT_NF, NUC_NF, UnixTimeStamp = (1,) * 5
    """
    CSV File Index List
    [0] - Commit Hash   [1] - Author Name           [2] - Author Date Unix Time Stamp
    [3] - Author Email  [4] - Author Commit Date    [5] - Commit Message
    [6] - Fix (Boolean) [7] - Commit Classification (Merge, Feature Addition, Corrective, None)
    [8] - Linked (X)    [9] - Contains Bug          [10] - Fixes
    [11] - NS (Number of Subsystems)
    [12] - ND ( Number of Directories)      [13] - NF (Number of Files)
    [14] - Entropy                          [15] - LA (Lines Added)             [16] - LD (Lines Deleted)
    [17] - List of Files Changed            [18] - LT (Lines of code in a file before the change)
    [19] - NDEV (Number of Developers changed file before)      [20] AGE - Average time interval between commit
    [21] - NUC (Number of Unique Change)    [22] - EXP (Developer Experiences)  [23] - REXP [24] - SEXP
    [25] - NPF (Number of Previous Fault of included file)      [26] - NPBC (Number of Previous Buggy Commit of developer)
    [27] - GLM Probability                  [28] - Repository ID
    """
    for row in targetMat:
        tempInstance = []
        NS = num(row[11])
        NF = num(row[13])
        Entropy = num(row[14])
        LA = num(row[15])
        LD = num(row[16])
        LT = num(row[18])
        NDEV = num(row[19])
        NUC = num(row[21])
        AGE = num(row[20])
        EXP = num(row[22])
        SEXP = num(row[24])
        UnixTimeStamp = num(row[2])

        if row[6] == 'True' or row[6] == 'TRUE':
            FIX = 1
        else:
            FIX = 0

        if row[9] == 'True' or row[9] == 'TRUE':
            BUGGY = 1
        else:
            BUGGY = 0

        if LT != 0:
            LA_LT = LA / LT
            LD_LT = LD / LT
        else:
            # Log (1) = 0
            LA_LT = 1
            LD_LT = 1
        if NF != 0:
            if LT == 0:
                LT_NF = 1
            else:
                LT_NF = LT / NF
            if NUC == 0:
                NUC_NF = 1
            else:
                NUC_NF = NUC / NF
        else:
            LT_NF = 1
            NUC_NF = 1


        tempInstance = []
        tempInstance.append(NS)
        tempInstance.append(NF)
        tempInstance.append(Entropy)
        tempInstance.append(LA_LT)
        tempInstance.append(LD_LT)
        tempInstance.append(LT_NF)
        tempInstance.append(NDEV)
        tempInstance.append(AGE)
        tempInstance.append(NUC_NF)
        tempInstance.append(EXP)
        tempInstance.append(SEXP)
        tempInstance.append(FIX)
        tempInstance.append(BUGGY)
        tempInstance.append(UnixTimeStamp)
        #print tempInstance
        floatMat.append(tempInstance)

    #find max and min value for each feature
    for row in floatMat:
        if maxNS < row[0]:
            maxNS = row[0]
        elif minNS > row[0]:
            minNS = row[0]
        if maxNF < row[1]:
            maxNF = row[1]
        elif minNF > row[1]:
            minNF = row[1]
        if maxEntropy < row[2]:
            maxEntropy = row[2]
        elif minEntropy > row[2]:
            minEntropy = row[2]
        if maxLA_LT < row[3]:
            maxLA_LT = row[3]
        elif minLA_LT > row[3]:
            minLA_LT = row[3]
        if maxLD_LT < row[4]:
            maxLD_LT = row[4]
        elif minLD_LT > row[4]:
            minLD_LT = row[4]
        if maxLT_NF < row[5]:
            maxLT_NF = row[5]
        elif minLT_NF > row[5]:
            minLT_NF = row[5]
        if maxNDEV < row[6]:
            maxNDEV = row[6]
        elif minNDEV > row[6]:
            minNDEV = row[6]
        if maxAGE < row[7]:
            maxAGE = row[7]
        elif minAGE > row[7]:
            minAGE = row[7]
        if maxNUC_NF < row[8]:
            maxNUC_NF = row[8]
        elif minNUC_NF > row[8]:
            minNUC_NF = row[8]
        if maxEXP < row[9]:
            maxEXP = row[9]
        elif minEXP > row[9]:
            minEXP = row[9]
        if maxSEXP < row[10]:
            maxSEXP = row[10]
        elif minSEXP > row[10]:
            minSEXP = row[10]
        if maxFIX < row[11]:
            maxFIX = row[11]
        elif minFIX > row[11]:
            minFIX = row[11]

        """
        floatMat Content
        [0] - NS    [1] - NF    [2] - Entropy   [3] - LA_LT
        [4] - LD_LT [5] - LT_NF [6] - NDEV      [7] - AGE
        [8] - NUC_NF    [9] - EXP   [10] - SEXP [11] - FIX
        [12] - BUGGY    [13] - TimeStamp

        """
    # max-min normalization process
    for row in floatMat:
        tempNS = row[0]
        tempNS = (tempNS - minNS) / minus(maxNS, minNS)
        tempNF = row[1]
        tempNF = (tempNF - minNF) / minus(maxNF, minNF)
        tempEntropy = row[2]
        tempEntropy = (tempEntropy - minEntropy) / minus(maxEntropy, minEntropy)
        tempLA = row[3]
        tempLA = (tempLA - minLA_LT) / minus(maxLA_LT, minLA_LT)
        tempLD = row[4]
        tempLD = (tempLD - minLD_LT) / minus(maxLD_LT, minLD_LT)
        tempLT = row[5]
        tempLT = (tempLT - minLT_NF) / minus(maxLT_NF, minLT_NF)
        tempNDEV = row[6]
        tempNDEV = (tempNDEV - minNDEV) / minus(maxNDEV, minNDEV)
        tempAGE = row[7]
        tempAGE = (tempAGE - minAGE) / minus(maxAGE, minAGE)
        tempNUC = row[8]
        tempNUC = (tempNUC - minNUC_NF) / minus(maxNUC_NF, minNUC_NF)
        tempEXP = row[9]
        tempEXP = (tempEXP - minEXP) / minus(maxEXP, minEXP)
        tempSEXP = row[10]
        tempSEXP = (tempSEXP - minSEXP) / minus(maxSEXP, minSEXP)
        tempFIX = row[11]
        tempFIX = (tempFIX - minFIX) / minus(maxFIX, minFIX)

        #attach normalized data instances into new matrix
        tempInstance = []
        tempInstance.append(tempNS)
        tempInstance.append(tempNF)
        tempInstance.append(tempEntropy)
        tempInstance.append(tempLA)
        tempInstance.append(tempLD)
        tempInstance.append(tempLT)
        tempInstance.append(tempNDEV)
        tempInstance.append(tempAGE)
        tempInstance.append(tempNUC)
        tempInstance.append(tempEXP)
        tempInstance.append(tempSEXP)
        tempInstance.append(tempFIX)
        tempInstance.append(row[12])
        tempInstance.append(row[13])
        retMat.append(tempInstance)
    return retMat


"""
CSV File Index List
[0] - Commit Hash   [1] - Author Name           [2] - Author Date Unix Time Stamp
[3] - Author Email  [4] - Author Commit Date    [5] - Commit Message
[6] - Fix (Boolean) [7] - Commit Classification (Merge, Feature Addition, Corrective, None)
[8] - Linked (X)    [9] - Contains Bug          [10] - Fixes
[11] - NS (Number of Subsystems)
[12] - ND ( Number of Directories)      [13] - NF (Number of Files)
[14] - Entropy                          [15] - LA (Lines Added)             [16] - LD (Lines Deleted)
[17] - List of Files Changed            [18] - LT (Lines of code in a file before the change)
[19] - NDEV (Number of Developers changed file before)      [20] AGE - Average time interval between commit
[21] - NUC (Number of Unique Change)    [22] - EXP (Developer Experiences)  [23] - REXP [24] - SEXP
[25] - NPF (Number of Previous Fault of included file)      [26] - NPBC (Number of Previous Buggy Commit of developer)
[27] - GLM Probability                  [28] - Repository ID
"""


# Used Metric /NS/NF/Entropy/LA_LT/LD_LT/LT_NF/NDEV/AGE/NUC_NF/EXP/SEXP/NPF/NPBC/FIX/BUGGY

def Logarithm_Normalization (targetMat) :
    retMat = []
    NS, NF, Entropy, LA, LD, LT, NDEV, AGE, NUC, EXP, SEXP, NPF, NPBC, FIX = (1,) * 14
    LA_LT, LD_LT, LT_NF, NUC_NF, UnixTimeStamp = (1,) * 5
    for row in targetMat:
        tempInstance = []
        NS = num(row[11])
        NF = num(row[13])
        Entropy = num(row[14])
        LA = num(row[15])
        LD = num(row[16])
        LT = num(row [18])
        NDEV = num(row[19])
        NUC = num(row[21])
        AGE = num(row[20])
        EXP = num(row[22])
        SEXP = num(row[24])
        # NPF = num(row[25])
        # NPBC = num(row[26])
        UnixTimeStamp = num(row[2])

        if row[6] == 'True' or row[6] == 'TRUE':
            FIX = 1
        else:
            FIX = 0

        if row[9] == 'True' or row[9] == 'TRUE':
            BUGGY = 1
        else:
            BUGGY = 0

        if LT != 0:
            LA_LT = LA / LT
            LD_LT = LD / LT
        else:
        # Log (1) = 0
            LA_LT = 1
            LD_LT = 1
        if NF != 0:
            if LT == 0:
                LT_NF = 1
            else:
                LT_NF = LT / NF
            if NUC == 0:
                NUC_NF = 1
            else:
                NUC_NF = NUC / NF
        else:
            LT_NF = 1
            NUC_NF = 1

        NS = log(NS)
        NF = log(NF)
        Entropy = log(Entropy)
        LA_LT = log(LA_LT)
        LD_LT = log(LD_LT)
        LT_NF = log(LT_NF)
        NDEV = log(NDEV)
        AGE = log(AGE)
        NUC_NF = log(NUC_NF)
        EXP = log(EXP)
        SEXP = log(SEXP)
        # NPF = log(NPF)
        # NPBC = log(NPBC)
        tempInstance.append(NS)
        tempInstance.append(NF)
        tempInstance.append(Entropy)
        tempInstance.append(LA_LT)
        tempInstance.append(LD_LT)
        tempInstance.append(LT_NF)
        tempInstance.append(NDEV)
        tempInstance.append(AGE)
        tempInstance.append(NUC_NF)
        tempInstance.append(EXP)
        tempInstance.append(SEXP)
        # tempInstance.append(NPF)
        # tempInstance.append(NPBC)
        tempInstance.append(FIX)
        tempInstance.append(BUGGY)
        #Time Stamp Index => 13
        tempInstance.append(UnixTimeStamp)
        retMat.append(tempInstance)
    return retMat
#Normalization process. Translate string to float value
def num(s):
    ret = 0
    try:
        ret = float(s)
    except:
        print sys.exc_info()[0]
        print ret
    return ret

def minus(max, min):
    if max == min:
        return 1
    else :
        return max - min

def log(x):
    # The author of TSE 2013 paper applied standard log transformation for normalization in data pre-processing
    try:
        ret = math.log10(x)
    except:
        ret = 0
    return ret
