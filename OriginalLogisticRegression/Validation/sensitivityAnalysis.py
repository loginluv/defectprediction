from GraduateResearch.OriginalLogisticRegression.ModelConstruction import LogisticRegressionModel
from GraduateResearch.Utils.randomSampling import RandomUnderSampling

def SensitivityAnalysis(targetData, trainPercent, fileName):
    accuracy = 0

    print "Sensitivity Analysis"
    # Iterable elements for 1st param, getKey function of each element (Unix Time Stamp)
    sortedMat = sorted(targetData, key=getKey)
    totalIndex = len(sortedMat)
    trainingIndex = int(len(sortedMat) * trainPercent)
    testPercent = trainPercent + 0.1
    testIndex = int(len(sortedMat) * testPercent)
    train = []
    test = []

    for i in range(0, trainingIndex):
        train.append(sortedMat[i])

    for i in range(trainingIndex, testIndex):
        test.append(sortedMat[i])

    # for i in range(trainingIndex, totalIndex):
    #     test.append(sortedMat[i])

    # Source Code for Re-sampling train data
    buggyMat = []
    cleanMat = []
    cleanCnt = 0
    buggyCnt = 0

    for row in train:
        # print row
        if row[12] == 1:
            buggyCnt = buggyCnt + 1
            buggyMat.append(row)
        else:
            cleanCnt = cleanCnt + 1
            cleanMat.append(row)
    print "Buggy Count : ", buggyCnt, "Clean Count : ", cleanCnt

    if buggyCnt > cleanCnt :
        reSampledMat = RandomUnderSampling(buggyMat, cleanCnt)
        reSampledMat.extend(cleanMat)
    elif buggyCnt < cleanCnt:
        reSampledMat = RandomUnderSampling(cleanMat, buggyCnt)
        reSampledMat.extend(buggyMat)
    else:
        reSampledMat = buggyMat
        reSampledMat.extend(cleanMat)

    trainCleanCnt = 0
    trainBuggyCnt = 0

    for row in reSampledMat:
        if row[12] == 1:
            trainBuggyCnt = trainBuggyCnt + 1
        else:
            trainCleanCnt = trainCleanCnt + 1

    testBuggyCnt = 0
    testCleanCnt = 0
    for row in test:
        if row[12] == 1:
            testBuggyCnt = testBuggyCnt + 1

        else:
            testCleanCnt = testCleanCnt + 1

    trainDataLength = len(reSampledMat)
    testDataLength = len(test)

    print "%s %d" % ("After Re-sampling Merged List Length : ", len(reSampledMat))

    print "%s %d %s %d" % ("Train Length : ", len(reSampledMat), "Test Length : ", len(test))
    print "Train Clean Cnt : ", str(trainCleanCnt), "Train Buggy Cnt : ", str(trainBuggyCnt), "Test Clean Cnt : ", str(testCleanCnt), "Test Buggy Cnt : ", str(testBuggyCnt)

    accuracy = []
    precision = []
    recall = []
    f1score = []
    auc = []

    for i in range(10):
        print i
        Accuracy, Precision, Recall, F1score, AUC = LogisticRegressionModel.LogisticRegressionModelConstruct(reSampledMat,
                                                                                                             test, fileName)
        accuracy.append(Accuracy)
        precision.append(Precision)
        recall.append(Recall)
        f1score.append(F1score)
        auc.append(AUC)
        # accuracy.append(LogisticRegressionModel.LogisticRegressionModelConstruct(train,test))

    print "Acc      Pre    Recall    F1     AUC"
    for i in range(10):
        print "%.2f\t%.2f\t%.2f\t%.2f\t%.2f" % (accuracy[i], precision[i], recall[i], f1score[i], auc[i])

    return trainDataLength, trainCleanCnt, trainBuggyCnt, testDataLength, testCleanCnt, testBuggyCnt, accuracy, precision, recall, f1score, auc


# Return each commit's unix timestamp for sorting
def getKey(dataInstance):
    return dataInstance[13]