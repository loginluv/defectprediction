import numpy as np
import tensorflow as tf
from sklearn.metrics import roc_auc_score

from Utils.ColumnFilter import ColumnFiltering


def ListToNumpyArray(targetData):
    inputMat = []
    outputMat = []
    # Used Metric /NS/NF/Entropy/LA_LT/LD_LT/LT_NF/NDEV/AGE/NUC_NF/EXP/SEXP/NPF/NPBC/FIX/BUGGY
    bias = []
    NS = []
    NF = []
    Entropy = []
    LA_LT = []
    LD_LT = []
    LT_NF = []
    NDEV = []
    AGE = []
    NUC_NF = []
    EXP = []
    SEXP = []
    NPF = []
    NPBC = []
    FIX = []
    mergedList = []
    for row in targetData:
        bias.append(1)
        NS.append(row[0])
        NF.append(row[1])
        Entropy.append(row[2])
        LA_LT.append(row[3])
        LD_LT.append(row[4])
        LT_NF.append(row[5])
        NDEV.append(row[6])
        AGE.append(row[7])
        NUC_NF.append(row[8])
        EXP.append(row[9])
        SEXP.append(row[10])
        NPF.append(row[11])
        NPBC.append(row[12])
        FIX.append(row[13])

        # Append Buggy Label to output list
        outputMat.append(row[14])
    """
    [0]-bias  [1]-NS     [2]-NF    [3]-Entropy  [4]-LA_LT
    [5]-LDLT  [6]-LTNF   [7]-NDEV  [8]-AGE      [9]-NUCNF
    [10]-EXP    [11]-SEXP   [12]-NPF    [13]-NPBC   [14]-FIX
    """
    mergedList.append(bias)
    mergedList.append(NS)
    mergedList.append(NF)
    mergedList.append(Entropy)
    mergedList.append(LA_LT)
    mergedList.append(LD_LT)
    mergedList.append(LT_NF)
    mergedList.append(NDEV)
    mergedList.append(AGE)
    mergedList.append(NUC_NF)
    mergedList.append(EXP)
    mergedList.append(SEXP)
    mergedList.append(NPF)
    mergedList.append(NPBC)
    mergedList.append(FIX)

    train_np = np.array(mergedList)

    inputMat = train_np[0:]

    outputMat = np.array(outputMat)

    return inputMat, outputMat


def ListToInstanceRow(targetData):
    # Insert bias to row data matrix
    targetBuffer = targetData
    biasInsertedMat = []

    for instance in targetBuffer:
        if len(instance) == 16:
            instance.insert(0, 1)
        biasInsertedMat.append(instance)

    npArray = np.array(biasInsertedMat)
    inputMat = []
    outputMat = []

    for row in npArray:
        # Input Matrix Form : bias/NS/NF/Entropy/LA_LT/LD_LT/LT_NF/NDEV/AGE/NUC_NF/EXP/SEXP/NPF/NPBC/FIX/BUGGY/TimeStamp
        inputMat.append(row[0:-2])
        outputMat.append(row[-2])

    return inputMat, outputMat

def SoftmaxOuputCreator (testData):
    softMax_train_output = []
    softMax_first = []
    softMax_second = []
    for row in testData:
        if row == 1:
            softMax_first = 1
            softMax_second = 0
        else:
            softMax_first = 0
            softMax_second = 1
        tempInstance = []
        tempInstance.append(softMax_first)
        tempInstance.append(softMax_second)
        softMax_train_output.append(tempInstance)

    return softMax_train_output

def SoftMaxClassification(trainData, testData, fileName):

    train_input_data, train_output_data = ListToInstanceRow(trainData)
    test_input_data, test_output_data = ListToInstanceRow(testData)

    softMax_train_output_data = SoftmaxOuputCreator(train_output_data)
    softMax_test_output_data = SoftmaxOuputCreator(test_output_data)

    """
    [0]-bias  [1]-NS     [2]-NF    [3]-Entropy  [4]-LA_LT
    [5]-LDLT  [6]-LTNF   [7]-NDEV  [8]-AGE      [9]-NUCNF
    [10]-EXP    [11]-SEXP   [12]-NPF    [13]-NPBC   [14]-FIX
    """

    # Same Index Order, No change
    tensorflow_RemoveIndex = [11]
    socketIO_RemoveIndex = [10]
    notepad_RemoveIndex = [11, 12]
    scikit_RemoveIndex = [9]
    django_RemoveIndex = [10]
    tesseract_RemoveIndex = [1, 2, 11]
    cmake_RemoveIndex = [11]
    cygwin_RemoveIndex = []
    docker_RemoveIndex = [6, 9]

    if fileName == 'tensorflow':
        removeMat = tensorflow_RemoveIndex
    elif fileName == 'tesseract':
        removeMat = tesseract_RemoveIndex
    elif fileName == 'scikit':
        removeMat = scikit_RemoveIndex
    elif fileName == 'socketio':
        removeMat = socketIO_RemoveIndex
    elif fileName == 'django':
        removeMat = django_RemoveIndex
    elif fileName == 'notepad':
        removeMat = notepad_RemoveIndex
    elif fileName == 'cmake':
        removeMat = cmake_RemoveIndex
    elif fileName == 'cygwin':
        removeMat = cygwin_RemoveIndex
    else:
        removeMat = docker_RemoveIndex

    # Choose which data set used for prediction

    trainInputFilteredMat = ColumnFiltering(train_input_data, removeMat)
    testInputFilteredMat = ColumnFiltering(test_input_data, removeMat)

    X = tf.placeholder(tf.float32, [None, len(trainInputFilteredMat)])
    Y = tf.placeholder(tf.float32, [None, 2])

    # Normal Logistic Regression Model. Init weights by using random uniform distribution
    W = tf.Variable(tf.random_uniform([len(trainInputFilteredMat), 1], -1.0, 1.0))

    pred = tf.nn.softmax(tf.matmul(X, W))
    cost = tf.reduce_mean(-tf.reduce_sum(Y * tf.log(pred), reduction_indices=1))

    #hypothesis = tf.div(1., 1.+tf.exp(-h))
    #cost = -tf.reduce_mean(Y*tf.log(hypothesis) + (1-Y)*tf.log(1-hypothesis))
    #cost = tf.reduce_mean(-tf.reduce_sum(Y*tf.log(hypothesis) + (1-Y)*tf.log(1-hypothesis)))

    a = tf.Variable(0.001)
    # optimizer = tf.train.GradientDescentOptimizer(a)
    optimizer = tf.train.AdamOptimizer(a)

    train = optimizer.minimize(cost)

    #init = tf.initialize_all_variables()
    init = tf.global_variables_initializer()

    sess = tf.Session()
    sess.run(init)
#
    for step in xrange(2000):
        sess.run(train, feed_dict={X:trainInputFilteredMat, Y:softMax_train_output_data})
        if step % 200 ==0:
            print step, sess.run(cost, feed_dict={X:trainInputFilteredMat, Y:softMax_train_output_data})


    #sess.run(train, feed_dict={X: trainInputFilteredMat, Y: train_output_data})
    #print sess.run(cost, feed_dict={X: trainInputFilteredMat, Y: train_output_data}), sess.run(W)
    results = sess.run(pred, feed_dict={X:testInputFilteredMat})
    correct_prediction = tf.equal(tf.argmax(pred, 1), tf.argmax(Y, 1))
    accuracy = tf.reduce_mean(tf.case(correct_prediction, tf.float32))
    print ("Accuracy: ", accuracy.eval({X:testInputFilteredMat, Y:softMax_test_output_data}))

    i = 0
    correct = 0
    total = 0
    TruePositive = 0
    TrueNegative = 0
    FalsePositive = 0
    FalseNegative = 0

    #print results
    for result in results[0]:
        if (result >= 0.5).all():
            #Answer = True, Prediction = Positive
            #TruePositive
            if test_output_data[i] == 1:
                correct += 1
                TruePositive +=1
            #Answer = False, Prediction = Positive
            #FalsePositive
            else:
                FalsePositive +=1

        elif (result <0.5).all():
            #Answer = False, Prediction = Negative
            if test_output_data[i] == 0:
                correct += 1
                TrueNegative += 1

            #Answer = True, Prediction = Negative
            else:
                FalseNegative += 1

        total += 1
        i+=1
#    accuracy = (correct / float(total)) * 100
    print 'TP = ' + str(TruePositive) + ", TN = " + str(TrueNegative) + ", FP = " \
          + str(FalsePositive) + ", FN = " + str(FalseNegative)

    print "Defective Change : ", TruePositive + FalseNegative, ", Non-Defective Change : ", FalsePositive + TrueNegative

    try:
        Accuracy = (TruePositive + TrueNegative) / float(TruePositive + TrueNegative + FalseNegative + FalsePositive) * 100
    except ZeroDivisionError:
        Accuracy = 0
    try:
        Recall = (TruePositive / float(TruePositive + FalseNegative)) * 100
    except ZeroDivisionError:
        Recall = 0
    try:
        Precision = (TruePositive / float(TruePositive + FalsePositive)) * 100
    except ZeroDivisionError:
        Precision = 0
    try:
        F1Score = 2 * Precision * Recall / float(Precision + Recall)
    except ZeroDivisionError:
        F1Score = 0

    print 'Total Count = ' + str(total) + ', Correct = ' + str(correct)
    print 'Accuracy = ' + str (Accuracy) + ', Precision = ' + str(Precision) + ', Recall = ' + str(Recall) + ', F1 Score = ' + str(F1Score)

    y_true = np.array(test_output_data)
    y_score = np.array(results[0])
    try:
        aucScore = roc_auc_score(y_true, y_score)
    except ValueError:
        aucScore = 0
    print 'AUC = ' + str(aucScore)
    return Accuracy, Precision, Recall, F1Score, aucScore
