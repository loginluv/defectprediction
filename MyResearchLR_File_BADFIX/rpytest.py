import rpy2.robjects as robject
from rpy2.robjects.packages import importr
import csv
import sys

filename = 'tesseract.csv'
f = open('./input/output_' + filename, 'r')

csvReader = csv.reader(f)

mat = []

for row in csvReader:
    if row[0] =='NS':
        continue
    else:
        tempMat=[float(i) for i in row]
        mat.append(tempMat)

print type(mat[0][2])
print type(csvReader)
print mat

r_matrix = []
r_matrix = robject.r.matrix(robject.vectors.FloatVector(mat), nrow = len(mat))
try :
    r_matrix = robject.r.matrix(robject.vectors.FloatVector(mat), nrow = len(mat))
except ValueError:
    print sys.exc_info()[0]


print r_matrix
print type(r_matrix)

r = robject.r

input = []
lib = importr('faraway')
r_return = r.round(3.145, digits=2)
# r_return = r.round(lib.vif(mat))
print r_return[0]