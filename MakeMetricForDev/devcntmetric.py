import csv
import operator 
import ast
import os

#fileList = ["tensorflow","docker","cygwin","cmake","socketio", "django","scikit","angular","react","bugzilla"]

fileList = ["tensorflow","docker","cygwin","cmake","socketio", "django", "scikit","react","postgre","hadoop"]

#f = open('notepad.csv', 'r')
#output = open('output2.csv', 'w')

LABELING = "DEV_C_C"
overallInfo = open('Overall_'+LABELING+'.csv', 'w')
csv_overall_writer = csv.writer(overallInfo, dialect="excel")

for filename in fileList :
    print "---------------------------------------"
    print "processing "+filename
    print "---------------------------------------"
    f = open(filename + '.csv', 'r')

    DIR =LABELING + "/"
    DEV_DIR = LABELING + "_DEV/"
    if not os.path.exists(DIR):
        os.mkdir(DIR)
    modifiedfilename = filename + "__"+LABELING

    if not os.path.exists(DEV_DIR):
        os.mkdir(DEV_DIR)
    fileinfodir = filename + "__"+LABELING +"_DEV"

    output = open(DIR + modifiedfilename + '.csv', 'w')
    #output = open('test.csv', 'w')
    fileInfo = open(DEV_DIR + fileinfodir + '.csv', 'w')

    rdr = csv.reader(f)
    rowlist = list(rdr)

    devlist = { }
    # Example devlist 
    # Tom [0][1][2][3]
    # [0] - bad fix count
    # [1] - fix count for badfix
    # [2] - fix count for DRE
    # [3] - Time
    # [4] - turnover count
    # [5] - Cummulative DRE
    # [6] - Current DRE
    defcnt = 0
    defpool = []
    mergeCnt = 0



    
    for i in reversed(range(0,len(rowlist))):
        # Bypass Merge commit
        if ( rowlist[i][7] == "Merge"):
            rowlist[i].append(0)
            rowlist[i].append(0)
            mergeCnt += 1
            continue

        # ---------------DRE PART -----------------------------
        # defect count if defect add fix hashsed to the pool
        if ( rowlist[i][9] == "TRUE" or rowlist[i][9] == "True"):
            hash_list = ast.literal_eval(rowlist[i][10])
            if type(hash_list) is list :
                for j in range(0, len(hash_list)):
                    # Check duplicate item
                    if hash_list[j] not in defpool:
                        defpool.append(hash_list[j])

        #check developer  and did fix?
        if ( rowlist[i][1] in devlist):
            # print rowlist[i][1] + str(devlist[rowlist[i][1]][3]) + "  " + str(int(rowlist[i][2])) 
            # Refresh if the activity length more than 3 month
            # 1month = 2592000     3month =7776000  6month = 15552000 1y = 31104000
            if (int(rowlist[i][2]) -  int(devlist[rowlist[i][1]][3]) > 7776000 ):
                # print "REFRESH!@!"
                # Refresh time
                devlist[rowlist[i][1]][3] = rowlist[i][2]

                # Refresh cummulative DRE
                devlist[rowlist[i][1]][5] = ((devlist[rowlist[i][1]][4])*devlist[rowlist[i][1]][5] + devlist[rowlist[i][1]][6])/float(devlist[rowlist[i][1]][4]+1)

                # Initialize fix count
                devlist[rowlist[i][1]][2] = 0

                # add turnover count
                devlist[rowlist[i][1]][4] += 1


            # if the developer fix, count +1
            if ( (rowlist[i][6] == "TRUE" or rowlist[i][6] == "True")  and (rowlist[i][8]=="TRUE" or rowlist[i][8]=="True") and (rowlist[i][0] in defpool)):
                devlist[rowlist[i][1]][2] += 1
                defpool.remove(rowlist[i][0])
                #print "# of Defect : " + str(len(defpool))

            # BADFIX COUNT
            # if the developer fix, count +1
            if ( (rowlist[i][6] == "TRUE" or rowlist[i][6] == "True")):
                devlist[rowlist[i][1]][1] += 1
                if ( rowlist[i][9] == "TRUE" or rowlist[i][9] == "True"):
                    devlist[rowlist[i][1]][0] += 1

        else:
            devlist[rowlist[i][1]] = [0,0,0,rowlist[i][2],0,0,0]
            if ( (rowlist[i][6] == "TRUE" or rowlist[i][6] == "True")  and (rowlist[i][8]=="TRUE" or rowlist[i][8]=="True") and (rowlist[i][0] in defpool)):
                devlist[rowlist[i][1]][2] += 1
                defpool.remove(rowlist[i][0])
                #print "# of Defect : " + str(len(defpool)) 
            # BADFIX COUNT
            if ( (rowlist[i][6] == "TRUE" or rowlist[i][6] == "True")):
                devlist[rowlist[i][1]][1] += 1
                if ( rowlist[i][9] == "TRUE" or rowlist[i][9] == "True"):
                    devlist[rowlist[i][1]][0] += 1

        # APPEND DRE to excel
        '''
        if len(defpool) != 0 :
            devlist[rowlist[i][1]][6] = devlist[rowlist[i][1]][2]/float(len(defpool))
            FinalDRE = ((devlist[rowlist[i][1]][4]*devlist[rowlist[i][1]][5])+devlist[rowlist[i][1]][6])/float(1+ devlist[rowlist[i][1]][4])
            rowlist[i].append(FinalDRE)
            # rowlist[i].append("Test")
        else:
            rowlist[i].append(0.5)
        '''
        rowlist[i].append(devlist[rowlist[i][1]][1])
        '''
        # APPEND BADFIX to excel
        if (devlist[rowlist[i][1]][1] == 0 ):
            #rowlist[i][29] = devlist[rowlist[i][1]][0]
            rowlist[i].append(0.5)
        else :
            rowlist[i].append( (devlist[rowlist[i][1]][0]/float(devlist[rowlist[i][1]][1])))

        rowlist[i].append(devlist[rowlist[i][1]][2])
        #rowlist[i].append(defcnt)
        rowlist[i].append(len(defpool))
        '''
        rowlist[i].append(devlist[rowlist[i][1]][0])

        rowlist[i].append(devlist[rowlist[i][1]][2])
        #rowlist[i].append(defcnt)
        rowlist[i].append(len(defpool))

    f.close()
    
    rowlist[0][29] = "DRE"
    rowlist[0][30] = "BADFIX"
    rowlist[0][31] = "Fix count"
    #rowlist[0][32] = "Cummulative Defect"
    rowlist[0][32] = "Defect pool"
    
    sorted_dict = sorted(devlist.items(), key=operator.itemgetter(1),reverse=True) 
    
    print "The number of developer : " + str(len(sorted_dict))
    # print "The list of developer who did fix change > 10 "
    #activeFixDev10 = 0
    #activeFixDev20 = 0
    #for dev in sorted_dict:
    #    if dev[1][1] >= 10 :
    #       activeFixDev10 += 1
    #    if dev[1][1] >= 20 :
    #       activeFixDev20 += 1
           #print dev[0] + ", " + str(dev[1])

    #print "The number of active fix developer(>10) :" + str(activeFixDev10)

    #print "The number of active fix developer(>20) :" + str(activeFixDev20)
    
    csv_writer = csv.writer(output, dialect="excel")
    
    for line in rowlist:
        csv_writer.writerow(line)
    print "Done"

    totBadfix = 0
    totFix = 0

    #for devname in devlist.keys():
    #    totBadfix += devlist[devname][0]
    #    totFix += devlist[devname][1]
    #print "Total commit : " + str(len(rowlist))
    #print "Buggy commit : " + str(defcnt)
    #print "Rate(buggy/totalcommit) : " + str(float(defcnt)/len(rowlist)) 
    #print "Badfix : " + str(totBadfix)
    #print "Fix : " + str(totFix)
    #print "Rate(badfix/fix) : "+str(float(totBadfix)/totFix)
    #print "Rate(badfix/totalcommit): " + str(float(totBadfix)/len(rowlist))
    #print "Rate(fix/totalcommit): " + str(float(totFix)/len(rowlist))
    #print "Done"
    output.close()

    
    #for dev in sorted_dict.keys():
    #    print dev + ":" + str(sorted_dict[dev])
