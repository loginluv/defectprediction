import csv
import operator 
import ast
import os

#fileList = ["tensorflow","docker","cygwin","cmake","socketio","tesseract", "django", "notepad", "scikit","angular","react","bugzilla","vscode","postgre","hadoop"]

fileList = ["tensorflow","docker","cygwin","cmake","socketio", "django", "scikit","react","postgre","hadoop"]
#fileList = ["test2"]
#fileList = ["socketio_test"]
#fileList = ["sample"]
LABELING = "NEW_HAVG6_HAVG"
overallInfo = open('Overall_'+LABELING+'.csv', 'w')
csv_overall_writer = csv.writer(overallInfo, dialect="excel")

for filename in fileList :
    print "---------------------------------------"
    print "processing "+filename
    print "---------------------------------------"
    f = open(filename + '.csv', 'r')
    # LABELING = "AAVG6__AAVG"
    DIR =LABELING + "/"    
    FILE_DIR = LABELING + "_FILE/"
    if not os.path.exists(DIR):
        os.mkdir(DIR)
    modifiedfilename = filename + "__"+LABELING

    if not os.path.exists(FILE_DIR):
        os.mkdir(FILE_DIR)
    fileinfodir = filename + "__"+LABELING +"_FILE"

    output = open(DIR + modifiedfilename + '.csv', 'w')
    #output = open('test.csv', 'w')
    fileInfo = open(FILE_DIR + fileinfodir + '.csv', 'w')

    rdr = csv.reader(f)
    rowlist = list(rdr)
    filelist = { }
    # Example filelist:
    # a.txt [0][1]
    # [0] - badfix
    # [1] - fix count for bad fix
    # [2] - fix count for DRE
    # [3] - Start time
    # [4] - Turnover Count
    # [5] - Cummulative DRE
    # [6] - Current DRE
    # [7] - DRE pool
    # [8] - Total defect
    # [9] - Total fix for DRE
    # [10] - Currnet BFR
    mergeCnt = 0    
    cnt = 0
    avgDRE = 0.0
    avgBFR = 0.0
    accumDRE = 0.0
    accumBFR = 0.0

    for i in reversed(range(1,len(rowlist))):
        bestBadfix = 0.0
        bestDRE = 100.0
        sumDRE = 0.0
        sumBadfix = 0.0
        #print str(cnt) + "th Commit :"
        # Bypass Merge commit
        if ( rowlist[i][7] == "Merge"):
            rowlist[i].append(0)
            rowlist[i].append(0)
            mergeCnt += 1
            continue
               
        filesinCommit = str(rowlist[i][17]).split(',')
        refineList = []
        for eachfile in filesinCommit:
            if ( eachfile == "CAS_DELIMITER"):
                continue
            refineList.append(eachfile)
            if( eachfile not in filelist):
                filelist[eachfile] = [0,0,0,rowlist[i][2],0,0,0,[],0,0,0]
       
        # DRE PART
        # defect count if defect add fix hashsed to the pool for each file
        if ( rowlist[i][9] == "TRUE" or rowlist[i][9] == "True"):
            hash_list = ast.literal_eval(rowlist[i][10])
            #print hash_list
            if type(hash_list) is list :
                for j in range(0, len(hash_list)):
                    for eachFile in refineList :
                        #print eachFile
                        # Check duplicate item
                        #if hash_list[j] not in filelist[eachFile][7]:
                            #print (hash_list[j] + "not in filelist")
                            # Search the file in hash, if it exists add 1
                        for ti in reversed(range(1,i)):
                            # print ((rowlist[ti][0]) )
                            if ( (rowlist[ti][0]) == (hash_list[j])):
                                #print ("Same" + rowlist[ti][0] + " to " + hash_list[j])
                                #print (str(rowlist[ti][17]))
                                if (eachFile in str(rowlist[ti][17])):
                                    filelist[eachFile][7].append(hash_list[j])
                                    filelist[eachFile][8]+=1
                                    #print(rowlist[i][0]+":::"+  "Added " + hash_list[j] + "to the " + eachFile +" length of the bugg is " + str(len(filelist[eachFile][7])))
                                break

        # Refresh if the activity length more than 3 month
        # 1month = 2592000     3month =7776000  6month = 15552000 1y = 31104000
        for eachFile in refineList:
            if (int(rowlist[i][2]) -  int(filelist[eachFile][3]) > 15552000 ):
                # Refresh time
                filelist[eachFile][3] = rowlist[i][2]
                # Refresh cummulative DRE
                filelist[eachFile][5] = ((filelist[eachFile][4])*filelist[eachFile][5] + filelist[eachFile][6])/float(filelist[eachFile][4]+1)
                # Initialize fix count
                filelist[eachFile][2] = 0
                # add turnover count
                filelist[eachFile][4] += 1

            # if the developer fix, count +1
            if ( (rowlist[i][6] == "TRUE" or rowlist[i][6] == "True")  and (rowlist[i][8]=="TRUE" or rowlist[i][8]=="True") and (rowlist[i][0] in filelist[eachFile][7])):
                filelist[eachFile][2] += 1
                filelist[eachFile][9] += 1
 
            # set 0.5 if the number of bug is 0
            if ( len(filelist[eachFile][7]) != 0 ):
                filelist[eachFile][6] = filelist[eachFile][2]/float(len(filelist[eachFile][7]))
            else : 
                filelist[eachFile][6] = 0.5
                #currentSUMDRE = 0.0
                #for v in filelist :
                #    currentSUMDRE += filelist[v][5]
                #filelist[eachFile][6] = currentSUMDRE/len(filelist)

            # if the developer fix, count +1
            if ( (rowlist[i][6] == "TRUE" or rowlist[i][6] == "True")  and (rowlist[i][8]=="TRUE" or rowlist[i][8]=="True") and (rowlist[i][0] in filelist[eachFile][7])):
                #print(rowlist[i][0]+"::: Fixed " + str(rowlist[i][0]) + "Total # of fix is " + str(filelist[eachFile][2]))
                #print("Before Fix " + str(filelist[eachFile][7]))
                filelist[eachFile][7].remove(rowlist[i][0])
                #print("After Fix " + str(filelist[eachFile][7]))
            FinalDRE = ((filelist[eachFile][4]*filelist[eachFile][5])+filelist[eachFile][6])/float( 1 + filelist[eachFile][4] )
            
            #if bestDRE > FinalDRE :
            #    bestDRE = FinalDRE    # DRE COUNT
            sumDRE += FinalDRE
        rowlist[i].append(sumDRE/float(len(refineList)))
        avgDRE += sumDRE/float(len(refineList))
        #rowlist[i].append(bestDRE)
        #rowlist[i].append(0)

        # Badfix part
        if ( (rowlist[i][6] == "TRUE" or rowlist[i][6] == "True") ):
            #print "Fix"
            for eachfile in refineList:
                filelist[eachfile][1] += 1
            if ( rowlist[i][9] == "TRUE" or rowlist[i][9] == "True"):
                #print "Bug"
                for eachfile in refineList:
                    filelist[eachfile][0] += 1 

        for eachfile in refineList:
            #print eachfile
            #print filelist[eachfile]
            #print "Badfix : " + str(filelist[eachfile][0])
            #print "Fix : "  + str(filelist[eachfile][1])
            if ( filelist[eachfile][1] ==0):
                #print (eachfile +"fix for badfix 0")
                commitBadfix = 0.5
                #SUMBADFIX = 0.0
                #for v in filelist :
                #    # print("CHECK")
                #    SUMBADFIX += filelist[v][10]
                #commitBadfix = SUMBADFIX/float(len(filelist))
            else :
                commitBadfix = filelist[eachfile][0]/float(filelist[eachfile][1])
                filelist[eachfile][10] = commitBadfix
            sumBadfix  += commitBadfix
            #if commitBadfix > bestBadfix:
            #    bestBadfix = commitBadfix
        bestBadfix = sumBadfix/ float(len(refineList)) 
        cnt+=1
        #rowlist[i].append(0)
        rowlist[i].append(bestBadfix)
        avgBFR += bestBadfix
        #rowlist[i].append(0)
        #print "Best Bad fix is " + str(bestBadfix)
        #print filelist[eachfile]
      
    f.close()
    rowlist[0].append(0)
    rowlist[0].append(0) 
    rowlist[0][29] = "DRE"
    rowlist[0][30] = "BADFIX"

    sumDefect = 0
    sumFix = 0
    avgDefect = 0.0
    avgFix =0.0
    sumBadFix = 0
    avgBadFix = 0.0
    topDefect =0
    topFix = 0
    zeroDefect = 0
    zeroFix = 0
    zeroBadFix = 0
    Non0sumDefect = 0
    Non0sumFix = 0
    Non0sumBadFix = 0
    csv_file_writer = csv.writer(fileInfo, dialect="excel")
    # csv_overall_writer = csv.writer(overallInfo, dialect="excel")
    print fileInfo
    csv_file_writer.writerow(["File","Total defect","Total Fix","Total bad fix","DRE","BADFIX"])
    csv_overall_writer.writerow([filename])
    csv_overall_writer.writerow(["Total # of files", "The # of 0 defect files", "The # of 0 fix files", "The #of 0 bad fix files", "Average defect","Average fix","Fix/Defect","Average Bad fix","Average defect in non 0","Average fix in non 0","Average Bad fix in non 0","Average DRE", "Average BFR"])
    for eachFile in filelist:
        #print eachFile
        #print "Total # defect :" + str(filelist[eachFile][8])
        #print "Total # fix : "   + str(filelist[eachFile][1])
        #print "Total # Bad fix : " + str(filelist[eachFile][0])
        csv_file_writer.writerow([eachFile,str(filelist[eachFile][8]),str(filelist[eachFile][1]),str(filelist[eachFile][0]), str(((filelist[eachFile][4]*filelist[eachFile][5])+filelist[eachFile][6])/float( 1 + filelist[eachFile][4] )),str(filelist[eachFile][10])])
        if (filelist[eachFile][8] == 0 ):
            zeroDefect += 1
        else:
            Non0sumDefect += filelist[eachFile][8]
        if (filelist[eachFile][9] == 0 ):
            zeroFix += 1
        else:
            Non0sumFix += filelist[eachFile][9]
        if (filelist[eachFile][0] == 0 ):
            zeroBadFix += 1
        else:
            Non0sumBadFix += filelist[eachFile][0]

        sumDefect += filelist[eachFile][8]
        sumFix  += filelist[eachFile][9]
        sumBadFix += filelist[eachFile][0]

        if ( filelist[eachFile][8] > topDefect ) :
            topDefect = filelist[eachFile][8]
        if ( filelist[eachFile][9] > topFix ) :
            topFix = filelist[eachFile][9]
    """
    print "Total number of files : " + str(len(filelist))
    print "The number of 0 defect files : " + str(zeroDefect) + "("+str(zeroDefect/float(len(filelist))) +")"
    print "The number of 0 fix files : " + str(zeroFix) + "("+str(zeroFix/float(len(filelist))) +")"
    print "The number of 0 bad fix files : " + str(zeroBadFix) + "("+str(zeroBadFix/float(len(filelist))) +")"
    print "Average defect : " + str(sumDefect/float(len(filelist)))
    print "Average fix : " + str(sumFix/float(len(filelist)))
    print "Fix/Defect : " + str(sumFix/float(sumDefect))
    print "Average Bad fix : " + str(sumBadFix/float(len(filelist)))

    print "Average defect in non zero: " + str(Non0sumDefect/float(zeroDefect))
    print "Average fix in non zero: " + str(Non0sumFix/float(zeroFix))
    print "Average Bad fix in non zero : " + str(Non0sumBadFix/float(zeroBadFix))

    print "Average DRE : " + str(avgDRE/float(len(filelist)))
    print "Average Bad fix rate : " + str(avgBFR/float(len(filelist)))

    print "Top defect : " + str(topDefect)
    print "Top fix : " + str(topFix)

    csv_overall_writer.writerow([str(len(filelist)),str(zeroDefect) + "("+str(zeroDefect/float(len(filelist))) +")",str(zeroFix) + "("+str(zeroFix/float(len(filelist))) +")",str(zeroBadFix) + "("+str(zeroBadFix/float(len(filelist))) +")",str(sumDefect/float(len(filelist))),str(sumFix/float(len(filelist))), str(sumFix/float(sumDefect)), str(sumBadFix/float(len(filelist))),str(Non0sumDefect/float(zeroDefect)),str(Non0sumFix/float(zeroFix)),str(Non0sumBadFix/float(zeroBadFix)),str(avgDRE/float(len(filelist))),str(avgBFR/float(len(filelist)))])
    #sorted_dict = sorted(devlist.items(), key=operator.itemgetter(1),reverse=True) 
    """
    csv_writer = csv.writer(output, dialect="excel")

    for line in rowlist:
        csv_writer.writerow(line)

    print "Done"
    
    output.close()
    fileInfo.close()
overallInfo.close()
