import csv
import operator 
import ast

fileList = ["django", "socketio", "docker", "tensorflow", "notepad", "tesseract", "cmake", "cygwin", "scikit","angular","react","bugzilla"]

#f = open('notepad.csv', 'r')
#output = open('output2.csv', 'w')

for filename in fileList :
    print "processing "+filename
    f = open(filename + '.csv', 'r')
    output = open(filename + '_test.csv', 'w')

    rdr = csv.reader(f)
    rowlist = list(rdr)

    devlist = { }
    # Example devlist 
    # Tom [0][1][2][3]
    # [0] - bad fix count
    # [1] - fix count for badfix
    # [2] - fix count for DRE
    # [3] - Time
    # [4] - turnover count
    # [5] - Cummulative DRE
    # [6] - Current DRE
    defcnt = 0
    defpool = []
    
    for i in reversed(range(0,len(rowlist))):
        # ---------------DRE PART -----------------------------
        # defect count if defect add fix hashsed to the pool
        if ( rowlist[i][9] == "TRUE" or rowlist[i][9] == "True"):
            hash_list = ast.literal_eval(rowlist[i][10])
            if type(hash_list) is list :
                for j in range(0, len(hash_list)):
                    # Check duplicate item
                    if hash_list[j] not in defpool:
                        defpool.append(hash_list[j])
            defcnt += 1
          
        #check developer  and did fix?
        if ( rowlist[i][1] in devlist):
            print rowlist[i][1] + str(devlist[rowlist[i][1]][3]) + "  " + str(int(rowlist[i][2])) 
            # Refresh if the activity length more than 3 month
            if (int(rowlist[i][2]) -  int(devlist[rowlist[i][1]][3]) > 7776000 ):
                print "REFRESH!@!"
                # Refresh time
                devlist[rowlist[i][1]][3] = rowlist[i][2]

                # Refresh cummulative DRE
                devlist[rowlist[i][1]][5] = ((devlist[rowlist[i][1]][4])*devlist[rowlist[i][1]][5] + devlist[rowlist[i][1]][6])/float(devlist[rowlist[i][1]][4]+1)

                # Initialize fix count
                devlist[rowlist[i][1]][2] = 0

                # add turnover count
                devlist[rowlist[i][1]][4] += 1


            # if the developer fix, count +1
            if ( (rowlist[i][6] == "TRUE" or rowlist[i][6] == "True")  and (rowlist[i][8]=="TRUE" or rowlist[i][8]=="True") and (rowlist[i][0] in defpool)):
                devlist[rowlist[i][1]][2] += 1
                defpool.remove(rowlist[i][0])
                #print "# of Defect : " + str(len(defpool))

            # BADFIX COUNT
            # if the developer fix, count +1
            if ( (rowlist[i][6] == "TRUE" or rowlist[i][6] == "True")):
                devlist[rowlist[i][1]][1] += 1
                if ( rowlist[i][9] == "TRUE" or rowlist[i][9] == "True"):
                    devlist[rowlist[i][1]][0] += 1

        else:
            devlist[rowlist[i][1]] = [0,0,0,rowlist[i][2],0,0,0]
            if ( (rowlist[i][6] == "TRUE" or rowlist[i][6] == "True")  and (rowlist[i][8]=="TRUE" or rowlist[i][8]=="True") and (rowlist[i][0] in defpool)):
                devlist[rowlist[i][1]][2] += 1
                defpool.remove(rowlist[i][0])
                #print "# of Defect : " + str(len(defpool)) 
            # BADFIX COUNT
            if ( (rowlist[i][6] == "TRUE" or rowlist[i][6] == "True")):
                devlist[rowlist[i][1]][1] += 1
                if ( rowlist[i][9] == "TRUE" or rowlist[i][9] == "True"):
                    devlist[rowlist[i][1]][0] += 1

        # APPEND DRE to excel
        if len(defpool) != 0 :
            devlist[rowlist[i][1]][6] = devlist[rowlist[i][1]][2]/float(len(defpool))
            FinalDRE = ((devlist[rowlist[i][1]][4]*devlist[rowlist[i][1]][5])+devlist[rowlist[i][1]][6])/float(1+ devlist[rowlist[i][1]][4])
            rowlist[i].append(FinalDRE)
            # rowlist[i].append("Test")
        else:
            rowlist[i].append(0)

        # APPEND BADFIX to excel
        if (devlist[rowlist[i][1]][1] == 0 ):
            #rowlist[i][29] = devlist[rowlist[i][1]][0]
            rowlist[i].append(0.5)
        else :
            rowlist[i].append( (devlist[rowlist[i][1]][0]/float(devlist[rowlist[i][1]][1])))

        rowlist[i].append(devlist[rowlist[i][1]][2])
        #rowlist[i].append(defcnt)
        rowlist[i].append(len(defpool))
    
    f.close()
    
    rowlist[0][29] = "DRE"
    rowlist[0][30] = "BADFIX"
    rowlist[0][31] = "Fix count"
    #rowlist[0][32] = "Cummulative Defect"
    rowlist[0][32] = "Defect pool"
    
    sorted_dict = sorted(devlist.items(), key=operator.itemgetter(1),reverse=True) 
    
    print "The number of developer : " + str(len(sorted_dict))
    print "The list of developer who did fix change > 10 "
    activeFixDev = 0
    for dev in sorted_dict:
        if dev[1] >= 20 :
           activeFixDev += 1
           #print dev[0] + ", " + str(dev[1])
    
    print "The number of active fix developer :" + str(activeFixDev)
    
    csv_writer = csv.writer(output, dialect="excel")
    
    for line in rowlist:
        csv_writer.writerow(line)
    print "Done"

    totBadfix = 0
    totFix = 0

    for devname in devlist.keys():
        totBadfix += devlist[devname][0]
        totFix += devlist[devname][1]
    print "Total commit : " + str(len(rowlist))
    print "badfix : " + str(totBadfix) +  " fix : " + str(totFix)
    print "Rate(bad/fix) : "+str(float(totBadfix)/totFix)
    print "Rate(bad/total): " + str(float(totBadfix)/len(rowlist))
    print "Rate(fix/total): " + str(float(totFix)/len(rowlist))
    print "Done"
    output.close()

    
    #for dev in sorted_dict.keys():
    #    print dev + ":" + str(sorted_dict[dev])
