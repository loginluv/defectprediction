import csv

fileList = ["django", "socketio", "docker", "tensorflow", "notepad", "tesseract", "cmake", "cygwin", "scikit"]
def num(s):
    ret = 0
    try:
        ret = float(s)
    except:
        ret = 0
    return ret


#fileList = ["django"]

for filename in fileList:
    f = open('Ad_Rate_CV_' + filename + '.csv', 'r')
    csvReader = csv.reader(f)
    meanAcc = 0
    meanPrec = 0
    meanRecall = 0
    meanF1 = 0
    meanAUC = 0    
    isFirst = True
    index = 0
    for line in csvReader:
        if len(line) == 0 :
            break
        if index > 50:
            break
        if isFirst :
            isFirst = False
            continue
        meanAcc += num(line[8])
        meanPrec += num(line[9])
        meanRecall += num(line[10])
        meanF1 += num(line[11])
        meanAUC += num(line[12])
        index += 1
    print filename + "Result"
    print "============================================="
    print "Accuracy : " + str(meanAcc/50.00)
    print "Precision: " + str(meanPrec/50.00)
    print "Recall   : " + str(meanRecall/50.00)
    print "F1       : " + str(meanF1/50.00)
    print "AUC      : " + str(meanAUC/50.00)
    print "============================================"
    f.close()
