import csv
import sys
from MyResearchLR.DataPreparation.normarlization import Logarithm_Normalization
from MyResearchLR.Validation.sensitivityAnalysis import SensitivityAnalysis
from MyResearchLR.Validation.crossValidation import CrossValidation

fileList = ["tensorflow", "docker", "cygwin", "cmake", "socketio", "tesseract","django","scikit","notepad", "react","angular","bugzilla"]

#fileList = ["tesseract"]
# fileList = ["scikit"]
# fileList = ["notepad"]
#fileList = ["notepad", "django"]
#fileList = ["django"]
#fileList = ["docker"]
# "docker", "socketio", "notepad", "scikit", "django", "tesseract", "cmake",

csv.field_size_limit(sys.maxsize)

for filename in fileList:
    matrix = []
    cleanMat = []
    buggyMat = []
    reSampledMat = []
    normalizedMat = []

    #filename = 'socketio'
    #f = open('../input/' + filename, 'r')
    f = open('../new_input/' + filename + '_BF_FLOAT_H.csv', 'r')

    csvReader = csv.reader(f)

    buggyCnt = 0
    cleanCnt = 0

    print "My research Normal Logistic Regression Model"

    """
    CSV File Index List
    [0] - Commit Hash   [1] - Author Name           [2] - Author Date Unix Time Stamp
    [3] - Author Email  [4] - Author Commit Date    [5] - Commit Message
    [6] - Fix (Boolean) [7] - Commit Classification (Merge, Feature Addition, Corrective, None)
    [8] - Linked (X)    [9] - Contains Bug          [10] - Fixes
    [11] - NS (Number of Subsystems)
    [12] - ND ( Number of Directories)      [13] - NF (Number of Files)
    [14] - Entropy                          [15] - LA (Lines Added)             [16] - LD (Lines Deleted)
    [17] - List of Files Changed            [18] - LT (Lines of code in a file before the change)
    [19] - NDEV (Number of Developers changed file before)      [20] AGE - Average time interval between commit
    [21] - NUC (Number of Unique Change)    [22] - EXP (Developer Experiences)  [23] - REXP [24] - SEXP
    [25] - NPF (Number of Previous Fault of included file)      [26] - NPBC (Number of Previous Buggy Commit of developer)
    [27] - GLM Probability                  [28] - Repository ID
    [29] - BAD FIX RATE
    """

    inputMat = []
    for row in csvReader:
        if row[0] != 'commit_hash':
            if row[9] == 'True' or row[9] == 'TRUE':
                buggyCnt = buggyCnt + 1
                buggyMat.append(row)
            else :
                cleanCnt = cleanCnt + 1
                cleanMat.append(row)
            inputMat.append(row)
    f.close()

    print filename
    print "%s %d %s %d" % ("Buggy Class : ", buggyCnt, ", Clean Class : ", cleanCnt)
    print "Total : ", buggyCnt+cleanCnt, "percent of buggy class : ", 100*buggyCnt/float(buggyCnt+cleanCnt)
    #Data random under sampling process
    # reSampledMat = RandomUnderSampling(cleanMat, buggyCnt)
    # reSampledMat.extend(buggyMat)
    # print "%s %d" % ("After Re-sampling Merged List Length : ", len(reSampledMat))

    logNormalizedMat = Logarithm_Normalization(inputMat)
    print "%s %d" % ("Normalized Mat Length : ", len(logNormalizedMat[0]))
    print "=================================="
    print " "

    # Write Code for writing Normalized Data into CSV File for VIF check in R studio
    #with open('../new_input/vif_' + filename + ".csv", "w") as file:
    #     csv_writer = csv.writer(file, dialect="excel")
    #     label = ["NS", "NF", "Entropy", "LA_LT", "LD_LT", "LT_NF", "NDEV", "AGE", "NUC_NF", "EXP", "SEXP", "NPF", "NPBC","FIX", "BUGGY", "TimeStamp"]
    #     csv_writer.writerow(label)#
    #     for line in logNormalizedMat:
    #         csv_writer.writerow(line)
    #print "Done"
    #file.close()

    # Block of code for Cross Validation

    newLine = ''
    csvStr = []

    trainDataLength, trainCleanCnt, trainBuggyCnt, testDataLength, testCleanCnt, testBuggyCnt = (0,) * 6

    for iter in range(5):
        print "===================================="
        print filename, " crossValidation, ", iter + 1, " iteration processing"
        print "===================================="
        trainDataLength, testDataLength, testCleanCnt, testBuggyCnt, Accuracy, Precision, Recall, F1, \
        AUC, TP, TN, FP, FN = CrossValidation(logNormalizedMat, 10, filename)
        for index in range(10):
            tempStr = []
            tempStr.append(trainDataLength)
            tempStr.append(testDataLength)
            tempStr.append(testCleanCnt)
            tempStr.append(testBuggyCnt)
            tempStr.append(TP[index])
            tempStr.append(TN[index])
            tempStr.append(FP[index])
            tempStr.append(FN[index])
            tempStr.append(Accuracy[index])
            tempStr.append(Precision[index])
            tempStr.append(Recall[index])
            tempStr.append(F1[index])
            tempStr.append(AUC[index])
            csvStr.append(tempStr)
    csvStr.append(newLine)

    with open('../new_input/CV_max_result/BADFIX/20170726/Ad_Rate_CV_' + filename + '.csv', "w") as file:
        csv_writer = csv.writer(file, dialect="excel")
        label = ["Train Length", "Test Length", "Test Clean Cnt",
                 "Test Buggy Cnt", "TP", "TN", "FP", "FN", "Accuracy", "Precision", "Recall", "F1", "AUC"]

        csv_writer.writerow(label)#

        for line in csvStr:
            csv_writer.writerow(line)

    print "Done"
    file.close()


    # Block of source code for autonomous sensitivity execution and writing into CSV file format
    # trainDataLength, trainCleanCnt, trainBuggyCnt, testDataLength, testCleanCnt, testBuggyCnt = (0,) * 6
    # for sensitivityIndex in range(5, 8):
    #     #sensitivity = (sensitivityIndex + 1) * 0.1
    #     sensitivity = (sensitivityIndex) * 0.1
    #
    #     #sensitivity = 0.6
    #     # repeat 3 times for quick experiment
    #     for iter in range(3):
    #         print "===================================="
    #         print filename, "file ", sensitivity, "sensitivity ", iter + 1, " iteration processing"
    #         print "===================================="
    #         trainDataLength, testDataLength, testCleanCnt, testBuggyCnt, Accuracy, Precision, Recall, F1, \
    #         AUC, TP, TN, FP, FN = SensitivityAnalysis(logNormalizedMat, sensitivity, filename)
    #         for index in range(10):
    #             tempStr = []
    #             tempStr.append(trainDataLength)
    #             tempStr.append(testDataLength)
    #             tempStr.append(testCleanCnt)
    #             tempStr.append(testBuggyCnt)
    #             tempStr.append(TP[index])
    #             tempStr.append(TN[index])
    #             tempStr.append(FP[index])
    #             tempStr.append(FN[index])
    #             tempStr.append(Accuracy[index])
    #             tempStr.append(Precision[index])
    #             tempStr.append(Recall[index])
    #             tempStr.append(F1[index])
    #             tempStr.append(AUC[index])
    #             csvStr.append(tempStr)
    #     csvStr.append(newLine)
    #
    # with open('../new_input/CV_max_result/Ad_LR_SA_' + filename + '.csv', "w") as file:
    #     csv_writer = csv.writer(file, dialect="excel")
    #     label = ["Train Length", "Test Length", "Test Clean Cnt",
    #              "Test Buggy Cnt", "TP", "TN", "FP", "FN", "Accuracy", "Precision", "Recall", "F1", "AUC"]
    #     csv_writer.writerow(label)
    #
    #     for line in csvStr:
    #         csv_writer.writerow(line)
    #
    # print "Done"
    # file.close()
