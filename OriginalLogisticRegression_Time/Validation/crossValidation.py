from OriginalLogisticRegression_Time.DataPreparation.randomSampling import RandomUnderSampling
from sklearn.cross_validation import train_test_split
from OriginalLogisticRegression_Time.ModelConstruction.LogisticRegressionModel import LogisticRegressionModelConstruct

def CrossValidation (targetMat, numOfFold, fileName):

    #train, test = train_test_split(targetMat, test_size=0.1)

    print "Cross Validation"
    splitedMat = []
    trainMat = targetMat
    testMat = []
    for index in range(9):
        divIndex = index
        trainIndex =10 - divIndex
        trainSize = 1 / float(trainIndex)
        trainMat, testMat = train_test_split(trainMat, test_size=trainSize)
        splitedMat.append(testMat)
    splitedMat.append(trainMat)

    accuracy = []
    precision = []
    recall = []
    f1score = []
    auc = []
    tp = []
    tn = []
    fp = []
    fn = []

    for i in range(9):
        test = splitedMat[i+1]
        train = []
        for index in range(i+1):
            train += splitedMat[index]

        # Source Code for Re-sampling train data
        buggyMat = []
        cleanMat = []
        cleanCnt = 0
        buggyCnt = 0

        for row in train:
            # [BUGGY] index is 12
            if row[12] == 1:
                buggyCnt = buggyCnt + 1
                buggyMat.append(row)
            else:
                cleanCnt = cleanCnt + 1
                cleanMat.append(row)
        print "Buggy Count : ", buggyCnt, "Clean Count : ", cleanCnt

        if buggyCnt > cleanCnt:
            reSampledMat = RandomUnderSampling(buggyMat, cleanCnt)
            reSampledMat.extend(cleanMat)
        elif buggyCnt < cleanCnt:
            reSampledMat = RandomUnderSampling(cleanMat, buggyCnt)
            reSampledMat.extend(buggyMat)
        else:
            reSampledMat = buggyMat
            reSampledMat.extend(cleanMat)
        print "%s %d" % ("After Re-sampling Merged List Length : ", len(reSampledMat))

        trainCleanCnt = 0
        trainBuggyCnt = 0
        for row in reSampledMat:
            if row[12] == 1:
                trainBuggyCnt = trainBuggyCnt + 1
            else:
                trainCleanCnt = trainCleanCnt + 1

        testBuggyCnt = 0
        testCleanCnt = 0

        for row in test:
            if row[12] == 1:
                testBuggyCnt = testBuggyCnt + 1

            else:
                testCleanCnt = testCleanCnt + 1

        trainDataLength = len(reSampledMat)
        testDataLength = len(test)

        Accuracy, Precision, Recall, F1score, AUC, TP, TN, FP, FN = LogisticRegressionModelConstruct(reSampledMat, test, fileName)
        accuracy.append(Accuracy)
        precision.append(Precision)
        recall.append(Recall)
        f1score.append(F1score)
        auc.append(AUC)
        tp.append(TP)
        tn.append(TN)
        fp.append(FP)
        fn.append(FN)

    print "Acc      Pre    Recall    F1     AUC"
    for i in range(9):
        print "%.2f\t%.2f\t%.2f\t%.2f\t%.2f" % (accuracy[i], precision[i], recall[i], f1score[i], auc[i])

    return trainDataLength, testDataLength, testCleanCnt, testBuggyCnt, accuracy, precision, recall, f1score, \
           auc, tp, tn, fp, fn

if __name__ == '__main__':
    dummyArr = [i for i in range(100)]
    dummy = CrossValidation(dummyArr, 10)

