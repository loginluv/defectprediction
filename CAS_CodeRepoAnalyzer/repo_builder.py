
from orm.repository import *
from caslogging import logging
import time
from db import *


class Repo_Builder():

    def __init__(self):
        logging.info("REPO Builder Constructor")

    def repoCreation(self, targetURL):
        logging.info("repo Instance Create")
        repoInstance = Repository(
            {'url': targetURL, 'status': 'Waiting to be Ingested'})
        print (repoInstance.url)
        print (repoInstance.status)
        return repoInstance
