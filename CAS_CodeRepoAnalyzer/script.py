"""
file: script.py
author: Christoffer Rosen <cbr4830@rit.edu>, Ben Grawi <bjg1568@rit.edu>
date: Jan. 2014
description: base script to call.
"""
from cas_manager import *
from simple_manager import *
from analyzer.analyzer import *
from orm.feedback import *  # so that we create the table - used by web
from orm.user import *  # so that we create the table - used by web
from orm.glmcoefficients import *  # so that we create the table - used by web
from repo_builder import *
import sys

if len(sys.argv) > 1:
    arg = sys.argv[1]
else:
    arg = ''

if arg == "initDb":
    # Init the database
    logging.info('Initializing the Database...')
    Base.metadata.create_all(engine)
    logging.info('Done')

else:
    logging.info("Starting CAS Manager")
    #target_URL = "https://github.com/AutoDo/AutoDo.git"
    #target_URL = "https://github.com/django/django.git"
    target_URL = sys.argv[1]

    #target_URL = "https://github.com/3kd1000/tensorflow.git"
    #target_URL = "https://github.com/eclipse/eclipse.git"
    repo_builder = Repo_Builder()
    repoInstance = repo_builder.repoCreation(target_URL)
    session = Session()

    #Refresh Repository for correct processing
    session.query(Repository).delete()
    session.add(repoInstance)
    #logging.info(session.new)
    session.commit()
    session.close()
    #cas_manager = CAS_Manager()
    #cas_manager.start()
    simple_manager = Simple_Manager()
    simple_manager.checkIngestion()
    simple_manager.checkAnalyzation()
    simple_manager.checkModel()
    simple_manager.checkBuildModel()
