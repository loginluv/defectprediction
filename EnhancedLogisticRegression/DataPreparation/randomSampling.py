import random

def RandomUnderSampling(majorityClass, targetCount):
    retMat = []
    currentCount = 0

    while(True):
        randomIndex = random.randrange(0, len(majorityClass))
        try:
            temporalRow = majorityClass.pop(randomIndex)
        #ValueError Raise when try to remove same index of element in list.
        except ValueError as e :
            #print(str(e))
            continue
        else:
            #add temporalRow at retMat and increase currentCount.
            retMat.append(temporalRow)
            currentCount = currentCount + 1
            #escape while loop when current count reach to target count
            if currentCount == targetCount:
                break

    return retMat