import math
import sys

"""
Should fix MixMax Normalization part for measures index before run program
CSV File Index information is accessible below MinMax Normalization Function
"""
def MinMax_Normalization (targetMat):
    retMat = []
    floatMat = []

    maxNS, minNS, maxND, minND, maxNF, minNF, maxEntropy, minEntropy, maxLA, minLA, maxLD, minLD = (1,) * 12
    maxLT, minLT, maxFIX, minFIX, maxNDEV, minNDEV, maxAGE, minAGE, maxNUC, minNUC, maxEXP, minEXP = (1,) * 12
    maxREXP, minREXP, maxSEXP, minSEXP = (1,) * 4

    #Change string list to int list
    for row in targetMat:
        tempInstance = []
        tempInstance.append(num(row[2]))
        tempInstance.append(num(row[3]))
        tempInstance.append(num(row[4]))
        tempInstance.append(num(row[5]))
        tempInstance.append(num(row[6]))
        tempInstance.append(num(row[7]))
        tempInstance.append(num(row[8]))
        tempInstance.append(num(row[9]))
        tempInstance.append(num(row[10]))
        tempInstance.append(num(row[11]))
        tempInstance.append(num(row[12]))
        tempInstance.append(num(row[13]))
        tempInstance.append(num(row[14]))
        tempInstance.append(num(row[15]))
        tempInstance.append(num(row[16]))

        floatMat.append(tempInstance)

    #find max and min value for each feature
    for row in floatMat:
        if maxNS < row[0]:
            maxNS = row[0]
        elif minNS > row[0]:
            minNS = row[0]
        if maxND < row[1]:
            maxND = row[1]
        elif minND > row[1]:
            minND = row[1]
        if maxNF < row[2]:
            maxNF = row[2]
        elif minNF > row[2]:
            minNF = row[2]
        if maxEntropy < row[3]:
            maxEntropy = row[3]
        elif minEntropy > row[3]:
            minEntropy = row[3]
        if maxLA < row[4]:
            maxLA = row[4]
        elif minLA > row[4]:
            minLA = row[4]
        if maxLD < row[5]:
            maxLD = row[5]
        elif minLD > row[5]:
            minLD = row[5]
        if maxLT < row[6]:
            maxLT = row[6]
        elif minLT > row[6]:
            minLT = row[6]
        if maxFIX < row[7]:
            maxFIX = row[7]
        elif minFIX > row[7]:
            minFIX = row[7]
        if maxNDEV < row[8]:
            maxNDEV = row[8]
        elif minNDEV > row[8]:
            minNDEV = row[8]
        if maxAGE < row[9]:
            maxAGE = row[9]
        elif minAGE > row[9]:
            minAGE = row[9]
        if maxNUC < row[10]:
            maxNUC = row[10]
        elif minNUC > row[10]:
            minNUC = row[10]
        if maxEXP < row[11]:
            maxEXP = row[11]
        elif minEXP > row[11]:
            minEXP = row[11]
        if maxREXP < row[12]:
            maxREXP = row[12]
        elif minREXP > row[12]:
            minREXP = row[12]
        if maxSEXP < row[13]:
            maxSEXP = row[13]
        elif minSEXP > row[13]:
            minSEXP = row[13]

    # max-min normalization process
    for row in floatMat:
        tempNS = row[0]
        tempNS = (tempNS - minNS) / minus(maxNS, minNS)
        tempND = row[1]
        tempND = (tempND - minND) / minus(maxND, minND)
        tempNF = row[2]
        tempNF = (tempNF - minNF) / minus(maxNF, minNF)
        tempEntropy = row[3]
        tempEntropy = (tempEntropy - minEntropy) / minus(maxEntropy, minEntropy)
        tempLA = row[4]
        tempLA = (tempLA - minLA) / minus(maxLA, minLA)
        tempLD = row[5]
        tempLD = (tempLD - minLD) / minus(maxLD, minLD)
        tempLT = row[6]
        tempLT = (tempLT - minLT) / minus(maxLT, minLT)
        tempFIX = row[7]
        tempFIX = (tempFIX - minFIX) / minus(maxFIX, minFIX)
        tempNDEV = row[8]
        tempNDEV = (tempNDEV - minNDEV) / minus(maxNDEV, minNDEV)
        tempAGE = row[9]
        tempAGE = (tempAGE - minAGE) / minus(maxAGE, minAGE)
        tempNUC = row[10]
        tempNUC = (tempNUC - minNUC) / minus(maxNUC, minNUC)
        tempEXP = row[11]
        tempEXP = (tempEXP - minEXP) / minus(maxEXP, minEXP)
        tempREXP = row[12]
        tempREXP = (tempREXP - minREXP) / minus(maxREXP, minREXP)
        tempSEXP = row[13]
        tempSEXP = (tempSEXP - minSEXP) / minus(maxSEXP, minSEXP)

        #attach normalized data instances into new matrix
        tempInstance = []
        tempInstance.append(tempNS)
        tempInstance.append(tempND)
        tempInstance.append(tempNF)
        tempInstance.append(tempEntropy)
        tempInstance.append(tempLA)
        tempInstance.append(tempLD)
        tempInstance.append(tempLT)
        tempInstance.append(tempFIX)
        tempInstance.append(tempNDEV)
        tempInstance.append(tempAGE)
        tempInstance.append(tempNUC)
        tempInstance.append(tempEXP)
        tempInstance.append(tempREXP)
        tempInstance.append(tempSEXP)
        #Append Last Column's value which is tagged label buggy or clean
        tempInstance.append(row[14])

        retMat.append(tempInstance)
    return retMat


"""
CSV File Index List
[0] - Commit Hash   [1] - Author Name           [2] - Author Date Unix Time Stamp
[3] - Author Email  [4] - Author Commit Date    [5] - Commit Message
[6] - Fix (Boolean) [7] - Commit Classification (Merge, Feature Addition, Corrective, None)
[8] - Linked (X)    [9] - Contains Bug          [10] - Fixes
[11] - NS (Number of Subsystems)
[12] - ND ( Number of Directories)      [13] - NF (Number of Files)
[14] - Entropy                          [15] - LA (Lines Added)             [16] - LD (Lines Deleted)
[17] - List of Files Changed            [18] - LT (Lines of code in a file before the change)
[19] - NDEV (Number of Developers changed file before)      [20] AGE - Average time interval between commit
[21] - NUC (Number of Unique Change)    [22] - EXP (Developer Experiences)  [23] - REXP [24] - SEXP
[25] - NPF (Number of Previous Fault of included file)      [26] - NPBC (Number of Previous Buggy Commit of developer)
[27] - GLM Probability                  [28] - Repository ID
"""


# Used Metric /NS/NF/Entropy/LA_LT/LD_LT/LT_NF/NDEV/AGE/NUC_NF/EXP/SEXP/NPF/NPBC/FIX/BUGGY

def Logarithm_Normalization (targetMat) :
    retMat = []

    NS, NF, Entropy, LA, LD, LT, NDEV, AGE, NUC, EXP, SEXP, NPF, NPBC, FIX = (1,) * 14
    LA_LT, LD_LT, LT_NF, NUC_NF, unixTimeStamp = (1,) * 5


    for row in targetMat:
        tempInstance = []
        NS = num(row[11])
        NF = num(row[13])
        Entropy = num(row[14])
        LA = num(row[15])
        LD = num(row[16])
        LT = num(row [18])
        NDEV = num(row[19])
        NUC = num(row[21])
        AGE = num(row[20])
        EXP = num(row[22])
        SEXP = num(row[24])
        NPF = num(row[25])
        NPBC = num(row[26])
        unixTimeStamp = num(row[2])

        if row[6] == 'True' or row[6] == 'TRUE':
            FIX = 1
        else:
            FIX = 0

        if row[9] == 'True' or row[9] == 'TRUE':
            BUGGY = 1
        else:
            BUGGY = 0

        if LT != 0:
            LA_LT = LA / LT
            LD_LT = LD / LT
        else:
        # Log (1) = 0
            LA_LT = 1
            LD_LT = 1
        if NF != 0:
            if LT == 0:
                LT_NF = 1
            else:
                LT_NF = LT / NF
            if NUC == 0:
                NUC_NF = 1
            else:
                NUC_NF = NUC / NF
        else:
            LT_NF = 1
            NUC_NF = 1

        try:
            NPBC = (NPBC / float(EXP)) * 100
        except ZeroDivisionError:
            NPBC = 1
        # Multiply 100 for transform NPF metric into percentage
        NPF = NPF * 100

        NS = log(NS)
        NF = log(NF)
        Entropy = log(Entropy)
        LA_LT = log(LA_LT)
        LD_LT = log(LD_LT)
        LT_NF = log(LT_NF)
        NDEV = log(NDEV)
        AGE = log(AGE)
        NUC_NF = log(NUC_NF)
        EXP = log(EXP)
        SEXP = log(SEXP)
        NPF = log(NPF)
        NPBC = log(NPBC)
        tempInstance.append(NS)
        tempInstance.append(NF)
        tempInstance.append(Entropy)
        tempInstance.append(LA_LT)
        tempInstance.append(LD_LT)
        tempInstance.append(LT_NF)
        tempInstance.append(NDEV)
        tempInstance.append(AGE)
        tempInstance.append(NUC_NF)
        tempInstance.append(EXP)
        tempInstance.append(SEXP)
        tempInstance.append(NPF)
        tempInstance.append(NPBC)
        tempInstance.append(FIX)
        tempInstance.append(BUGGY)
        tempInstance.append(unixTimeStamp)
        retMat.append(tempInstance)
    return retMat
#Normalization process. Translate string to float value
def num(s):
    ret = 0
    try:
        ret = float(s)
    except:
        print sys.exc_info()[0]
    return ret

def minus(max, min):
    if max == min:
        return 1
    else :
        return max - min

def log(x):
    # The author of TSE 2013 paper applied standard log transformation for normalization in data pre-processing
    try:
        ret = math.log10(x)
    except:
        ret = 0
    return ret