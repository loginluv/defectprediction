import csv

def CliffDelta():
    #fileList = ["tensorflow", "socketio", "notepad", "scikit", "django", "tesseract", "cmake", "cygwin", "docker"]
    #filename = 'socketio'
    #fileList = ["cmake", "tensorflow", "socketio", "cygwin", "docker", "tesseract"]
    fileList = ["django", "socketio", "docker", "tensorflow", "notepad", "tesseract", "cmake", "cygwin", "scikit"]
    cliffDeltaResultMat = []

    for filename in fileList:

        f = open('../new_input/KCSE_cliff/cliff_' + filename + '.csv', 'r')

        csvReader = csv.reader(f)

        h_accuracy = []
        h_precision = []
        h_recall = []
        h_f1score = []
        h_auc = []

        o_accuracy = []
        o_precision = []
        o_recall = []
        o_f1score = []
        o_auc = []

        index = 1

        csvMat = []
        for row in csvReader:
            index += 1
            csvMat.append(row)
        f.close()

        h_o_Large_accuracy = 0
        h_o_Small_accuracy = 0
        h_o_Large_precision = 0
        h_o_Small_precision = 0
        h_o_Large_recall = 0
        h_o_Small_recall = 0
        h_o_Large_f1score = 0
        h_o_Small_f1score = 0
        h_o_Large_auc = 0
        h_o_Small_auc = 0

        #for i in range(103, 153):

        for i in range(1, 51):
            if csvMat[i][1] != '':
                #print csvMat[i][8], csvMat[i][9]
                # h_accuracy.append(num(csvMat[i][8]))
                h_precision.append(num(csvMat[i][0]))
                h_recall.append(num(csvMat[i][1]))
                h_f1score.append(num(csvMat[i][2]))
                h_auc.append(num(csvMat[i][3]))

                # o_accuracy.append(num(csvMat[i][13]))
                o_precision.append(num(csvMat[i][4]))
                o_recall.append(num(csvMat[i][5]))
                o_f1score.append(num(csvMat[i][6]))
                o_auc.append(num(csvMat[i][7]))

        for i in range(50):
            for j in range(50):
                # if h_accuracy[i] > o_accuracy[j] :
                #     h_o_Large_accuracy += 1
                # elif h_accuracy[i] < o_accuracy[j]:
                #     h_o_Small_accuracy += 1

                if h_precision[i] > o_precision[j] :
                    h_o_Large_precision += 1
                elif h_precision[i] < o_precision[j]:
                    h_o_Small_precision += 1

                #print "Value of Recall, Hybrid : ", h_recall[i], ", Origin : ", o_recall[j]
                if h_recall[i] > o_recall[j]:
                    h_o_Large_recall += 1
                    #print "Type I === Ad : ", h_recall[i], ", Or : ", o_recall[j]

                elif h_recall[i] < o_recall[j]:
                    h_o_Small_recall += 1
                    #print "Type II === Ad : ", h_recall[i], ", Or : ", o_recall[j]

                if h_f1score[i] > o_f1score[j]:
                    h_o_Large_f1score += 1
                elif h_f1score[i] < o_f1score[j]:
                    h_o_Small_f1score += 1

                if h_auc[i] > o_auc[j]:
                    h_o_Large_auc += 1
                elif h_auc[i] < o_auc[j]:
                    h_o_Small_auc += 1

        print "File Name : ", filename
        print "total length of each matrix : " , len(h_recall), "Hybrid > Origin : " , h_o_Large_auc, "Hybrid < Origin : " , h_o_Small_auc

        cliffDelta_Precision = formula(h_o_Large_precision, h_o_Small_precision, 50, 50)
        cliffDelta_Recall = formula(h_o_Large_recall, h_o_Small_recall, 50, 50)
        cliffDelta_F1 = formula(h_o_Large_f1score, h_o_Small_f1score, 50, 50)
        cliffDelta_AUC = formula(h_o_Large_auc, h_o_Small_auc, 50, 50)

        tempInstance = []
        tempInstance.append(cliffDelta_Precision)
        tempInstance.append(cliffDelta_Recall)
        tempInstance.append(cliffDelta_F1)
        tempInstance.append(cliffDelta_AUC)
        cliffDeltaResultMat.append(tempInstance)

        print "Precision : ", cliffDelta_Precision, ", Recall : ", cliffDelta_Recall, ", F1 Score : ", cliffDelta_F1, ", AUC : ", cliffDelta_AUC
        print "==========================================="


def formula(NumOfLarger, NumOfSmaller, SampleSizeOfFirst, SampleSizeOfSecond):

    #cliffDelta = Substraction(NumOfLarger, NumOfSmaller) / float(SampleSizeOfFirst * SampleSizeOfSecond)
    cliffDelta = (NumOfLarger - NumOfSmaller) / float(SampleSizeOfFirst * SampleSizeOfSecond)

    return cliffDelta


def Substraction(x, y):
    if x > y :
        return x - y
    elif x < y :
        return y - x
    else :
        return 0

def num(s):
    ret = 0
    try:
        ret = float(s)
    except:
        ret = 0
    return ret



if __name__ == '__main__':
    CliffDelta()

