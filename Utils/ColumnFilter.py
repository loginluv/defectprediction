
def ColumnFiltering(targetData, removeIndexList):

    retMat = []
    # print "ColumnFiltering"
    # print "Original Data Length ", len(targetData)
    # print "Remove Index List"
    # print removeIndexList
    for columnIndex in range(len(targetData)):

        if not columnIndex in removeIndexList:
            #print "Here"
            retMat.append(targetData[columnIndex])

    return retMat