import numpy as np
import tensorflow as tf

from EnhancedLogisticRegression.WeighInitialization import Xavier


def ModifiedVersion (trainData, testData):
    # tf Graph Input
    x = tf.placeholder(tf.float32, [None, 14])  # input data consist of 14 features
    y = tf.placeholder(tf.float32)  # 0~1 logistic regression => 10 classes

    #Train Data Processing
    temp_arr = np.array(trainData)
    x_arr = []
    y_arr = []
    for tmp in temp_arr:
        x_arr.append(tmp[0:-1])
        y_arr.append(tmp[-1])

    #Test Data Processing
    test_arr = np.array(testData)
    x_test_arr = []
    y_test_arr = []
    for temp in test_arr:
        x_test_arr.append(temp[0:-1])
        y_test_arr.append(temp[-1])

    W1 = tf.get_variable("W1", shape=[14, 20], initializer=Xavier.xavier_init(14, 20))
    W2 = tf.get_variable("W2", shape=[20, 12], initializer=Xavier.xavier_init(20, 12))
    W3 = tf.get_variable("W3", shape=[12, 12], initializer=Xavier.xavier_init(12, 12))
    W4 = tf.get_variable("W4", shape=[12, 1], initializer=Xavier.xavier_init(12, 1))

    b1 = tf.Variable(tf.random_normal([20]))
    b2 = tf.Variable(tf.random_normal([12]))
    b3 = tf.Variable(tf.random_normal([12]))
    b4 = tf.Variable(tf.random_normal([1]))

    # Construct model #Logistic Regression
    L1 = tf.nn.relu(tf.matmul(x, W1) + b1)
    L2 = tf.nn.relu(tf.matmul(L1, W2) + b2)
    L3 = tf.nn.relu(tf.matmul(L2, W3) + b3)

    # Logistic Regression Classifier and Cost Function
    h = tf.add(tf.matmul(L3, W4), b4)
    hypothesis = tf.div(1., 1. + tf.exp(-h))

    cost = -tf.reduce_mean(y * tf.log(hypothesis) + (1 - y) * tf.log(1 - hypothesis))
    # Adam Optimizer
    optimizer = tf.train.AdamOptimizer(learning_rate=0.001).minimize(cost)

    init = tf.initialize_all_variables()

    with tf.Session() as sess:
        sess.run(init)
        for epoch in range(10):
            total_batch = len(x_arr)

        for step in xrange(2001):
            sess.run(optimizer, feed_dict={x: x_arr, y: y_arr})
            if step % 20 == 0:
                print step, sess.run(cost, feed_dict={x: x_arr, y: y_arr})

        # Test model
        correct_prediction = tf.equal(tf.argmax(hypothesis, 1), tf.argmax(y, 1))
        # Calculate accuracy
        accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
        #print "Accuracy:", accuracy.eval({x: x_test_arr, y: y_test_arr})

    return accuracy

if __name__ == '__main__':
    dummyArr = [i for i in range(10) for j in range(5)]
    dummy = ModifiedVersion(dummyArr, dummyArr)

